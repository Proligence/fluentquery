﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using NUnit.Framework;
    using Proligence.Astoria.Client.Wcf;
    using Proligence.FluentQuery.Client.Contract;
    using Proligence.FluentQuery.Client.Wcf;
    using ProtoBuf;

    /// <summary>
    /// Implements unit test for the <see cref="FluentQuery{T}"/> delegate.
    /// </summary>
    [TestFixture]
    public class FluentQueryUnitTests
    {
        [UsesFluentQuery]
        [ServiceContract]
        internal interface IFakeContract
        {
        }

        /// <summary>
        /// Tests if the <see cref="FluentQuery{T}"/> delegate is serialized correctly.
        /// </summary>
        [Test]
        public void TestSerialization()
        {
            FluentQuery<TestQuery> fluentQuery = q => q.Switch1().Text("Test");

            var fakeEndpoint = new ServiceEndpoint(new ContractDescription("test") { ContractType = typeof(IFakeContract) });
            fakeEndpoint.SetupDefaultEndpoint();
            fakeEndpoint.SetupFluentQueryForEndpoint();

            FluentQuery<TestQuery> deserializedQuery;
            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, fluentQuery);
                stream.Flush();
                stream.Seek(0L, SeekOrigin.Begin);

                deserializedQuery = Serializer.Deserialize<FluentQuery<TestQuery>>(stream);
            }

            TestQuery queryPart1 = (TestQuery)deserializedQuery(null);
            StringQueryPart stringPart = (StringQueryPart)queryPart1.ParentPart;
            Assert.That(stringPart.Name, Is.EqualTo("Text"));
            Assert.That(stringPart.Value, Is.EqualTo("Test"));

            TestQuery queryPart2 = (TestQuery)stringPart.ParentPart;
            SwitchQueryPart switchPart = (SwitchQueryPart)queryPart2.ParentPart;
            Assert.That(switchPart.Name, Is.EqualTo("Switch"));
            Assert.That(switchPart.Enabled, Is.True);

            TestQuery queryPart3 = (TestQuery)switchPart.ParentPart;
            Assert.That(queryPart3.ParentPart, Is.Null);
        }

        /// <summary>
        /// Test query part.
        /// </summary>
        [DataContract]
        [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", 
            Justification = "Used through reflection.")]
        private class TestQuery : QueryPart
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestQuery"/> class.
            /// </summary>
            public TestQuery()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TestQuery"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            public TestQuery(QueryPart parentPart) 
                : base(parentPart)
            {
            }

            /// <summary>
            /// Test switch 1.
            /// </summary>
            /// <returns>The fluent query.</returns>
            public TestQuery Switch1()
            {
                return new TestQuery(new SwitchQueryPart(this, "Switch", true));
            }

            /// <summary>
            /// Test value.
            /// </summary>
            /// <param name="str">The string value.</param>
            /// <returns>The fluent query.</returns>
            public TestQuery Text(string str)
            {
                return new TestQuery(new StringQueryPart(this, "Text", str));
            }

            /// <summary>
            /// Returns a <see cref="string"/> which represents the contents of the query part.
            /// </summary>
            /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
            [ExcludeFromCodeCoverage]
            public override string GetContentString()
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Test value query part which stores a <see cref="string"/>.
        /// </summary>
        [DataContract]
        [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses",
            Justification = "Used through reflection.")]
        private class StringQueryPart : ValueQueryPart<string>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="StringQueryPart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="value">The value of the query part.</param>
            public StringQueryPart(QueryPart parentPart, string name, string value)
                : base(parentPart, name, value)
            {
            }
        }
    }
}