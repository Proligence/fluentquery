﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListQueryPartUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="ListQueryPart{T}"/> class.
    /// </summary>
    [TestFixture]
    public class ListQueryPartUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="ListQueryPart{T}"/> class properly initializes a new instance.
        /// </summary>
        [Test]
        public void TestCtor()
        {
            var parent = new Mock<QueryPart>(null, null).Object;
            var part = new StringListQueryPart(parent, "MyPart", new[] { "one", "two", "three" });

            Assert.That(part.ParentPart, Is.SameAs(parent));
            Assert.That(part.Name, Is.EqualTo("MyPart"));
            Assert.That(part.Values, Is.EqualTo(new[] { "one", "two", "three" }));
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.GetContentString"/> method works correctly when
        /// <see cref="ListQueryPart{T}.Values"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenValuesNull()
        {
            var part = new StringListQueryPart(null, "MyPart", null);
            
            string result = part.GetContentString();
            
            Assert.That(result, Is.EqualTo("null"));
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.GetContentString"/> method works correctly when
        /// <see cref="ListQueryPart{T}.Values"/> contains zero values.
        /// </summary>
        [Test]
        public void GetContentStringWhenValuesEmpty()
        {
            var part = new StringListQueryPart(null, "MyPart", new string[0]);

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo(string.Empty));
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.GetContentString"/> method works correctly when
        /// <see cref="ListQueryPart{T}.Values"/> contains one value.
        /// </summary>
        [Test]
        public void GetContentStringWhenSingleValue()
        {
            var part = new StringListQueryPart(null, "MyPart", new[] { "value" });

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("value"));
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.GetContentString"/> method works correctly when
        /// <see cref="ListQueryPart{T}.Values"/> contains multiple values.
        /// </summary>
        [Test]
        public void GetContentStringWhenMultipleValues()
        {
            var part = new StringListQueryPart(null, "MyPart", new[] { "one", "two", "three" });

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("one,two,three"));
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.GetContentString"/> method works correctly when
        /// <see cref="ListQueryPart{T}.Values"/> contains a <c>null</c> value.
        /// </summary>
        [Test]
        public void GetContentStringWhenValuesContainsNull()
        {
            var part = new StringListQueryPart(null, "MyPart", new[] { "one", null, "three" });

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("one,,three"));
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.CompareContent"/> method works correctly when the content of the
        /// specified query part is equal to the content of the current query part.
        /// </summary>
        [Test]
        public void CompareContentWhenEqual()
        {
            var part1 = new StringListQueryPart(null, "MyPart", new[] { "one", "two", "three" });
            var part2 = new StringListQueryPart(null, "MyPart", new[] { "one", "two", "three" });

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.CompareContent"/> method works correctly when
        /// <see cref="ListQueryPart{T}.Values"/> is <c>null</c> for both query parts.
        /// </summary>
        [Test]
        public void CompareContentWhenEqualNull()
        {
            var part1 = new StringListQueryPart(null, "MyPart", null);
            var part2 = new StringListQueryPart(null, "MyPart", null);

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.CompareContent"/> method works correctly when
        /// <see cref="ListQueryPart{T}.Values"/> is <c>null</c> for one of the query parts.
        /// </summary>
        [Test]
        public void CompareContentWhenNotEqualNull()
        {
            var part1 = new StringListQueryPart(null, "MyPart", new[] { "one", "two", "three" });
            var part2 = new StringListQueryPart(null, "MyPart", null);

            Assert.That(part1.CompareContent(part2), Is.False);
            Assert.That(part2.CompareContent(part1), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.CompareContent"/> method works correctly when the content of the
        /// specified query part is equal to the content of the current query part and
        /// <see cref="ListQueryPart{T}.Values"/> contains a <c>null</c> value.
        /// </summary>
        [Test]
        public void CompareContentWhenEqualWithNullValue()
        {
            var part1 = new StringListQueryPart(null, "MyPart", new[] { "one", null, "three" });
            var part2 = new StringListQueryPart(null, "MyPart", new[] { "one", null, "three" });

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.CompareContent"/> method works correctly when the specified query
        /// part contains different values than the current query part.
        /// </summary>
        [Test]
        public void CompareContentWhenDifferentValues()
        {
            var part1 = new StringListQueryPart(null, "MyPart", new[] { "one", "two", "three" });
            var part2 = new StringListQueryPart(null, "MyPart", new[] { "one", "two" });

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.CompareContent"/> method works correctly when the specified query
        /// part contains different values than the current query part, but the same number of values..
        /// </summary>
        [Test]
        public void CompareContentWhenDifferentValuesButSameLength()
        {
            var part1 = new StringListQueryPart(null, "MyPart", new[] { "one", "two", "three" });
            var part2 = new StringListQueryPart(null, "MyPart", new[] { "one", "two", "four" });

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ListQueryPart{T}.CompareContent"/> method works correctly when the specified query
        /// part is not a <see cref="ListQueryPart{T}"/>.
        /// </summary>
        [Test]
        public void CompareContentWhenDifferentQueryPartType()
        {
            var part1 = new StringListQueryPart(null, "MyPart", new[] { "1", "2", "3" });
            var part2 = new IntegerListQueryPart(null, "MyPart", new[] { 1, 2, 3 });

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }
    }
}