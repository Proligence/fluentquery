﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NoValueQueryPartUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System.Runtime.Serialization;
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="NoValueQueryPart"/> class.
    /// </summary>
    [TestFixture]
    public class NoValueQueryPartUnitTests
    {
        /// <summary>
        /// Test if the constructor of the <see cref="NoValueQueryPart"/> class works correctly.
        /// </summary>
        [Test]
        public void CreateInstance()
        {
            var part = new TestNoValueQueryPart();

            Assert.That(part.Name, Is.Null);
            Assert.That(part.ParentPart, Is.Null);
        }

        /// <summary>
        /// Test if the constructor of the <see cref="NoValueQueryPart"/> class works correctly when a parent query
        /// part is specified.
        /// </summary>
        [Test]
        public void CreateInstanceWithParent()
        {
            var parent = new Mock<QueryPart>(null, null).Object;
            var part = new TestNoValueQueryPart(parent);

            Assert.That(part.ParentPart, Is.SameAs(parent));
        }

        /// <summary>
        /// Tests if the <see cref="NoValueQueryPart.GetContentString"/> method works correctly.
        /// </summary>
        [Test]
        public void TestGetContentString()
        {
            var part = new TestNoValueQueryPart();

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("TestNoValueQueryPart"));
        }

        /// <summary>
        /// Implements a subclass of <see cref="NoValueQueryPart"/> for test purposes.
        /// </summary>
        [DataContract]
        private class TestNoValueQueryPart : NoValueQueryPart
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestNoValueQueryPart"/> class.
            /// </summary>
            public TestNoValueQueryPart()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TestNoValueQueryPart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            public TestNoValueQueryPart(QueryPart parentPart)
                : base(parentPart)
            {
            }
        }
    }
}