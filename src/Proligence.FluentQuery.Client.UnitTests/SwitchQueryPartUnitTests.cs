﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SwitchQueryPartUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="SwitchQueryPart"/> class.
    /// </summary>
    [TestFixture]
    public class SwitchQueryPartUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="SwitchQueryPart"/> class properly initializes an enabled switch.
        /// </summary>
        [Test]
        public void TestCtorWhenEnabled()
        {
            var parent = new Mock<QueryPart>(null, null).Object;
            var part = new SwitchQueryPart(parent, "MySwitch", true);

            Assert.That(part.ParentPart, Is.EqualTo(parent));
            Assert.That(part.Name, Is.EqualTo("MySwitch"));
            Assert.That(part.Enabled, Is.True);
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="SwitchQueryPart"/> class properly initializes a disabled switch.
        /// </summary>
        [Test]
        public void TestCtorWhenDisabled()
        {
            var parent = new Mock<QueryPart>(null, null).Object;
            var part = new SwitchQueryPart(parent, "MySwitch", false);

            Assert.That(part.ParentPart, Is.EqualTo(parent));
            Assert.That(part.Name, Is.EqualTo("MySwitch"));
            Assert.That(part.Enabled, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="SwitchQueryPart.GetContentString"/> method returns the correct result when
        /// <see cref="SwitchQueryPart.Enabled"/> is <c>true</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenEnabled()
        {
            var part = new SwitchQueryPart(null, "MySwitch", true);

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("True"));
        }

        /// <summary>
        /// Tests if the <see cref="SwitchQueryPart.GetContentString"/> method returns the correct result when
        /// <see cref="SwitchQueryPart.Enabled"/> is <c>false</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenNotEnabled()
        {
            var part = new SwitchQueryPart(null, "MySwitch", false);

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("False"));
        }

        /// <summary>
        /// Tests if the <see cref="SwitchQueryPart.CompareContent"/> method works properly when the specified query
        /// part is equal to the current query part.
        /// </summary>
        /// <param name="enabled">The value of the <see cref="SwitchQueryPart.Enabled"/> property.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void CompareContentWhenQueryPartEqual(bool enabled)
        {
            var part1 = new SwitchQueryPart(null, "MySwitch1", enabled);
            var part2 = new SwitchQueryPart(null, "MySwitch2", enabled);

            Assert.That(part1.CompareContent(part2), Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="SwitchQueryPart.CompareContent"/> method works properly when the specified query
        /// part is not equal to the current query part.
        /// </summary>
        [Test]
        public void CompareContentWhenQueryPartNotEqual()
        {
            var part1 = new SwitchQueryPart(null, "MySwitch1", true);
            var part2 = new SwitchQueryPart(null, "MySwitch2", false);

            Assert.That(part1.CompareContent(part2), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="SwitchQueryPart.CompareContent"/> method works properly when the specified query
        /// part is not equal to the current query part.
        /// </summary>
        [Test]
        public void CompareContentWhenQueryPartNotSwitch()
        {
            var part1 = new SwitchQueryPart(null, "MySwitch", true);
            var part2 = new Mock<QueryPart>(null, null).Object;

            Assert.That(part1.CompareContent(part2), Is.False);
        }
    }
}