﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FinalQueryPartUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="FinalQueryPart"/> class.
    /// </summary>
    [TestFixture]
    public class FinalQueryPartUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="FinalQueryPart"/> class properly initializes a new instance when
        /// <c>null</c> parent part is specified.
        /// </summary>
        [Test]
        public void TestCtorWhenParentPartNull()
        {
            var part = new FinalQueryPart();
            
            Assert.That(part.Name, Is.Null);
            Assert.That(part.ParentPart, Is.Null);
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="FinalQueryPart"/> class properly initializes a new instance when
        /// a parent part is specified.
        /// </summary>
        [Test]
        public void TestCtorWhenParentPartNotNull()
        {
            var parent = new Mock<QueryPart>(null, null).Object;
            var part = new FinalQueryPart(parent);

            Assert.That(part.Name, Is.Null);
            Assert.That(part.ParentPart, Is.SameAs(parent));
        }

        /// <summary>
        /// Tests if the <see cref="FinalQueryPart.GetContentString"/> method works correctly.
        /// </summary>
        [Test]
        public void TestGetContentString()
        {
            var part = new FinalQueryPart();

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo(string.Empty));
        }

        /// <summary>
        /// Tests if the <see cref="FinalQueryPart.CompareContent"/> method works correctly when the specified query
        /// part is a <see cref="FinalQueryPart"/>.
        /// </summary>
        [Test]
        public void CompareContentWhenFinalQueryPart()
        {
            var part = new FinalQueryPart();

            bool result = part.CompareContent(new FinalQueryPart());

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="FinalQueryPart.CompareContent"/> method works correctly when the specified query
        /// part is not a <see cref="FinalQueryPart"/>.
        /// </summary>
        [Test]
        public void CompareContentWhenNotFinalQueryPart()
        {
            var part = new FinalQueryPart();

            bool result = part.CompareContent(new Mock<QueryPart>(null, null).Object);

            Assert.That(result, Is.False);
        }
    }
}