﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryKnownTypesUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Runtime.Serialization;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;
    using Proligence.FluentQuery.Client.Wcf;

    /// <summary>
    /// Implements unit tests for the <see cref="FluentQueryKnownTypes"/> class.
    /// </summary>
    [TestFixture]
    public class FluentQueryKnownTypesUnitTests
    {
        /// <summary>
        /// Tests if the result of the <see cref="FluentQueryKnownTypes.GetKnownTypes"/> method contains the
        /// <c>System.DelegateSerializationHolder+DelegateEntry</c> type.
        /// </summary>
        [Test]
        public void GetKnownTypesResultContainsDelegateEntry()
        {
            Type delegateEntryType = Type.GetType("System.DelegateSerializationHolder+DelegateEntry");

            IEnumerable<Type> result = FluentQueryKnownTypes.GetKnownTypes(null);

            Assert.That(result.Contains(delegateEntryType));
        }

        /// <summary>
        /// Tests if the result of the <see cref="FluentQueryKnownTypes.GetKnownTypes"/> method contains all query
        /// part types.
        /// </summary>
        [Test]
        public void GetKnownTypesResultContainsQueryPartTypes()
        {
            IEnumerable<Type> queryPartTypes = FluentQueryKnownTypes.GetQueryPartTypes();

            IEnumerable<Type> result = FluentQueryKnownTypes.GetKnownTypes(null);

            foreach (Type queryPartType in queryPartTypes)
            {
                Assert.That(result.Contains(queryPartType));
            }
        }

        /// <summary>
        /// Tests if the result of the <see cref="FluentQueryKnownTypes.GetQueryPartTypes"/> method contains the
        /// subclasses of <see cref="QueryPart"/>.
        /// </summary>
        [Test]
        public void GetQueryPartTypesResultContainsQueryPartSubclasses()
        {
            IEnumerable<Type> result = FluentQueryKnownTypes.GetQueryPartTypes();

            Assert.That(result.Contains(typeof(DummyQueryPart)));
        }

        /// <summary>
        /// Dummy query part.
        /// </summary>
        [DataContract]
        internal class DummyQueryPart : QueryPart
        {
            /// <summary>
            /// Returns a <see cref="string"/> which represents the contents of the query part.
            /// </summary>
            /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
            [ExcludeFromCodeCoverage]
            public override string GetContentString()
            {
                throw new NotImplementedException();
            }
        }
    }
}