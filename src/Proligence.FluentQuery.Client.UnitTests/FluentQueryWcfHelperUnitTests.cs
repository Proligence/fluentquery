﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryWcfHelperUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using NUnit.Framework;
    using Proligence.Astoria.Client.Wcf;
    using Proligence.FluentQuery.Client.Wcf;
    using ProtoBuf.ServiceModel;

    /// <summary>
    /// Implements unit tests for the <see cref="FluentQueryWcfHelper"/> class.
    /// </summary>
    [TestFixture]
    public class FluentQueryWcfHelperUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="FluentQueryWcfHelper.SetupFluentQueryForServiceHost"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified <see cref="ServiceHostBase"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void SetupFluentQueryForServiceHostWhenHostNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => FluentQueryWcfHelper.SetupFluentQueryForServiceHost(null));

            Assert.That(exception.ParamName, Is.EqualTo("serviceHost"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryWcfHelper.SetupFluentQueryForServiceHost"/> method correctly sets up
        /// a service host with no endpoints.
        /// </summary>
        [Test]
        public void SetupFluentQueryForServiceHostWhenZeroEndpoints()
        {
            using (var serviceHost = new ServiceHost(typeof(object)))
            {
                serviceHost.SetupFluentQueryForServiceHost();
            }
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryWcfHelper.SetupFluentQueryForServiceHost"/> method correctly sets up
        /// a service host with one endpoint.
        /// </summary>
        [Test]
        public void SetupFluentQueryForServiceHostWhenOneEndpoint()
        {
            var contract = new ContractDescription("Contract") { ContractType = typeof(string) };
            
            var operation = new OperationDescription("Op1", contract);
            contract.Operations.Add(operation);
            
            var endpoint = new ServiceEndpoint(contract)
            {
                Contract = contract,
                Address = new EndpointAddress("http://dummy"),
                Binding = new BasicHttpBinding()
            };

            var service = new ServiceDescription(new[] { endpoint });

            using (ServiceHostBase serviceHost = this.CreateServiceHost(service))
            {
                serviceHost.SetupFluentQueryForServiceHost();
                serviceHost.SetupDefaultServiceHost();

                var dcs1 = operation.Behaviors.Find<ProtoOperationBehavior>();
                Assert.That(dcs1, Is.Not.Null);
            }
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryWcfHelper.SetupFluentQueryForServiceHost"/> method correctly sets up
        /// a service host with many endpoints.
        /// </summary>
        [Test]
        public void SetupFluentQueryForServiceHostWhenManyEndpoints()
        {
            var contract1 = new ContractDescription("Contract1") { ContractType = typeof(string) };
            
            var operation1 = new OperationDescription("Op1", contract1);
            contract1.Operations.Add(operation1);

            var endpoint1 = new ServiceEndpoint(contract1)
            {
                Contract = contract1,
                Address = new EndpointAddress("http://dummy"),
                Binding = new BasicHttpBinding()
            };

            var contract2 = new ContractDescription("Contract") { ContractType = typeof(string) };

            var operation2 = new OperationDescription("Op2", contract2);
            contract2.Operations.Add(operation2);

            var endpoint2 = new ServiceEndpoint(contract2)
            {
                Contract = contract2,
                Address = new EndpointAddress("http://dummy"),
                Binding = new BasicHttpBinding()
            };

            var service = new ServiceDescription(new[] { endpoint1, endpoint2 });

            using (ServiceHostBase serviceHost = this.CreateServiceHost(service))
            {
                serviceHost.SetupFluentQueryForServiceHost();
                serviceHost.SetupDefaultServiceHost();

                var dcs1 = operation1.Behaviors.Find<ProtoOperationBehavior>();
                Assert.That(dcs1, Is.Not.Null);

                var dcs2 = operation2.Behaviors.Find<ProtoOperationBehavior>();
                Assert.That(dcs2, Is.Not.Null);
            }
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryWcfHelper.SetupFluentQueryForEndpoint"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified endpoint is <c>null</c>.
        /// </summary>
        [Test]
        public void SetupFluentQueryForEndpointWhenEndpointNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => FluentQueryWcfHelper.SetupFluentQueryForEndpoint(null));

            Assert.That(exception.ParamName, Is.EqualTo("endpoint"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryWcfHelper.SetupFluentQueryForEndpoint"/> method correctly sets up the
        /// specified endpoint.
        /// </summary>
        [Test]
        public void SetupFluentQueryForEndpointWhenDataContractSerializerOperationBehaviorNotInEndpoint()
        {
            var contract = new ContractDescription("Contract")
            {
                ContractType = typeof(FluentQueryUnitTests.IFakeContract)
            };

            var operation1 = new OperationDescription("Op1", contract);
            contract.Operations.Add(operation1);

            var operation2 = new OperationDescription("Op2", contract);
            contract.Operations.Add(operation2);

            var endpoint = new ServiceEndpoint(contract) { Contract = contract };

            endpoint.SetupDefaultEndpoint();
            endpoint.SetupFluentQueryForEndpoint();

            var dcs1 = operation1.Behaviors.Find<ProtoOperationBehavior>();
            Assert.That(dcs1, Is.Not.Null);

            var dcs2 = operation2.Behaviors.Find<ProtoOperationBehavior>();
            Assert.That(dcs2, Is.Not.Null);
        }

        /// <summary>
        /// Creates a service host stub.
        /// </summary>
        /// <param name="contract">The test service contract.</param>
        /// <returns>The created <see cref="ServiceHostBase"/> instance.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification = "By design.")]
        private ServiceHostBase CreateServiceHost(ServiceDescription contract)
        {
            var service = new ServiceHost(typeof(string));
            
            // ReSharper disable PossibleNullReferenceException
            typeof(ServiceHostBase)
                .GetField("description", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(service, contract);
            //// ReSharper restore PossibleNullReferenceException

            return service;
        }
    }
}