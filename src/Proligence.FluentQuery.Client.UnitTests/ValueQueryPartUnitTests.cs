﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValueQueryPartUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="ValueQueryPart{T}"/> class.
    /// </summary>
    [TestFixture]
    public class ValueQueryPartUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="ValueQueryPart{T}"/> class correctly initializes a query part
        /// which contains a <see cref="string"/>.
        /// </summary>
        [Test]
        public void TestStringPairCtor()
        {
            var parent = new Mock<QueryPart>(null, null).Object;
            var part = new StringQueryPart(parent, "MyValue", "MyStr");

            Assert.That(part.ParentPart, Is.EqualTo(parent));
            Assert.That(part.Name, Is.EqualTo("MyValue"));
            Assert.That(part.Value, Is.EqualTo("MyStr"));
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.GetContentString"/> works correctly when the value stored in the
        /// query part is <c>null</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenValueNull()
        {
            var part = new StringQueryPart(null, "MyValue", null);

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("null"));
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.GetContentString"/> works correctly when the value stored in the
        /// query part is not <c>null</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenValueNotNull()
        {
            var part = new StringQueryPart(null, "MyValue", "MyStr");

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("MyStr"));
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.CompareContent"/> method works correctly when the value query
        /// part's contents are equal.
        /// </summary>
        [Test]
        public void CompareContentWhenQueryPartValuesEqual()
        {
            var part1 = new StringQueryPart(null, "MyValue", "MyStr");
            var part2 = new StringQueryPart(null, "MyValue", "MyStr");

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.CompareContent"/> method works correctly when the value of the
        /// specified query part is not equal to the value of the current query part and both values are not
        /// <c>null</c>.
        /// </summary>
        [Test]
        public void CompareContentWhenQueryPartValuesNotEqual()
        {
            var part1 = new StringQueryPart(null, "MyValue", "MyStr1");
            var part2 = new StringQueryPart(null, "MyValue", "MyStr2");

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.CompareContent"/> method works correctly when the specified query
        /// part is not a <see cref="ValueQueryPart{T}"/>.
        /// </summary>
        [Test]
        public void CompareContentWhenNotValueQueryPart()
        {
            var part = new StringQueryPart(null, "MyValue", "MyStr");

            bool result = part.CompareContent(new Mock<QueryPart>(null, null).Object);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.CompareContent"/> method works correctly when the value of the
        /// specified query part is <c>null</c>.
        /// </summary>
        [Test]
        public void CompareContentWhenValueNullCase1()
        {
            var part1 = new StringQueryPart(null, "MyValue", "MyStr");
            var part2 = new StringQueryPart(null, "MyValue", null);

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.CompareContent"/> method works correctly when the value of the
        /// current query part is <c>null</c>.
        /// </summary>
        [Test]
        public void CompareContentWhenValueNullCase2()
        {
            var part1 = new StringQueryPart(null, "MyValue", null);
            var part2 = new StringQueryPart(null, "MyValue", "MyStr");

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ValueQueryPart{T}.CompareContent"/> method works correctly when the value of the
        /// specified and current query part is <c>null</c>.
        /// </summary>
        [Test]
        public void CompareContentWhenBothValuesNull()
        {
            var part1 = new StringQueryPart(null, "MyValue", null);
            var part2 = new StringQueryPart(null, "MyValue", null);

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.True);
        }
    }
}