﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueryUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System;
    using System.Collections.Generic;
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;
    using Proligence.FluentQuery.Client.Wcf;

    /// <summary>
    /// Implements unit tests for the <see cref="Query"/> class.
    /// </summary>
    [TestFixture]
    public class QueryUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="Query"/> class properly initializes a new instance when the
        /// specified arguments are valid.
        /// </summary>
        [Test]
        public void TestCtorWhenValidArgs()
        {
            var root = new Mock<QueryPart>(null, null).Object;
            var query = new Query(root);

            Assert.That(query.RootQueryPart, Is.SameAs(root));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="Query"/> class throws a <see cref="ArgumentNullException"/>
        /// when the specified root query part is null.
        /// </summary>
        [Test]
        public void TestCtorWhenRootPartNotNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new Query(null));

            Assert.AreEqual(exception.ParamName, "rootQueryPart");
        }
    }
}