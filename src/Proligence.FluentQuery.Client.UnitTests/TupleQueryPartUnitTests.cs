﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TupleQueryPartUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System.Runtime.Serialization;
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="TupleQueryPart{T1,T2}"/> class.
    /// </summary>
    [TestFixture]
    public class TupleQueryPartUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="TupleQueryPart{T1,T2}"/> class correctly initializes a pair of
        /// <see cref="string"/> objects.
        /// </summary>
        [Test]
        public void TestStringPairCtor()
        {
            var parent = new Mock<QueryPart>(null, null).Object;
            var part = new TestTupleQueryPart(parent, "MyTuple", "Str1", "Str2");

            Assert.That(part.ParentPart, Is.EqualTo(parent));
            Assert.That(part.Name, Is.EqualTo("MyTuple"));
            Assert.That(part.Value1, Is.EqualTo("Str1"));
            Assert.That(part.Value2, Is.EqualTo("Str2"));
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.GetContentString"/> method works correctly when both values
        /// of the query part are not null.
        /// </summary>
        [Test]
        public void GetContentStringWhenBothValuesNotNull()
        {
            var part = new TestTupleQueryPart(null, "MyTuple", "Str1", "Str2");

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("Str1,Str2"));
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.GetContentString"/> method works correctly when both values
        /// of the query part are <c>null</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenBothValuesNull()
        {
            var part = new TestTupleQueryPart(null, "MyTuple", null, null);

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo(","));
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.GetContentString"/> method works correctly when the first
        /// value of the query part is <c>null</c> and the other is not <c>null</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenFirstValueNull()
        {
            var part = new TestTupleQueryPart(null, "MyTuple", null, "Str2");

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo(",Str2"));
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.GetContentString"/> method works correctly when the first
        /// value of the query part is <c>null</c> and the other is not <c>null</c>.
        /// </summary>
        [Test]
        public void GetContentStringWhenSecondValueNull()
        {
            var part = new TestTupleQueryPart(null, "MyTuple", "Str1", null);

            string result = part.GetContentString();

            Assert.That(result, Is.EqualTo("Str1,"));
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.CompareContent"/> method works correctly when the content of
        /// the specified query part is equal to the content of the current query part.
        /// </summary>
        /// <param name="value1">The first value.</param>
        /// <param name="value2">The second value.</param>
        [TestCase("Str1", "Str2")]
        [TestCase(null, "Str2")]
        [TestCase("Str1", null)]
        [TestCase(null, null)]
        public void CompareContentWhenEqual(string value1, string value2)
        {
            var part1 = new TestTupleQueryPart(null, "MyTuple", value1, value2);
            var part2 = new TestTupleQueryPart(null, "MyTuple", value1, value2);

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.CompareContent"/> method works correctly when the first value
        /// of the specified query part is not equal to the first value of the current query part.
        /// </summary>
        /// <param name="value">The first value of the current query part.</param>
        /// <param name="other">The first value of the other query part.</param>
        [TestCase("Str1", "Str2")]
        [TestCase("Str1", null)]
        [TestCase(null, "Str1")]
        public void CompareContentWhenFirstValueNotEqual(string value, string other)
        {
            var part1 = new TestTupleQueryPart(null, "MyTuple", value, "value");
            var part2 = new TestTupleQueryPart(null, "MyTuple", other, "value");

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.CompareContent"/> method works correctly when the second
        /// value of the specified query part is not equal to the second value of the current query part.
        /// </summary>
        /// <param name="value">The first value of the current query part.</param>
        /// <param name="other">The first value of the other query part.</param>
        [TestCase("Str1", "Str2")]
        [TestCase("Str1", null)]
        [TestCase(null, "Str1")]
        public void CompareContentWhenSecondValueNotEqual(string value, string other)
        {
            var part1 = new TestTupleQueryPart(null, "MyTuple", "value", value);
            var part2 = new TestTupleQueryPart(null, "MyTuple", "value", other);

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="TupleQueryPart{T1,T2}.CompareContent"/> method works correctly when the specified
        /// query part is not a <see cref="TupleQueryPart{T1,T2}"/>.
        /// </summary>
        [Test]
        public void CompareContentWhenDifferentQueryPartType()
        {
            var part1 = new TestTupleQueryPart(null, "MyTuple", "value1", "value2");
            var part2 = new SwitchQueryPart(null, "name", true);

            bool result = part1.CompareContent(part2);

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Test tuple query part.
        /// </summary>
        [DataContract]
        private class TestTupleQueryPart : TupleQueryPart<string, string>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestTupleQueryPart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="value1">The first value which will be stored in the query part.</param>
            /// <param name="value2">The second value which will be stored in the query part.</param>
            public TestTupleQueryPart(QueryPart parentPart, string name, string value1, string value2)
                : base(parentPart, name, value1, value2)
            {
            }
        }
    }
}