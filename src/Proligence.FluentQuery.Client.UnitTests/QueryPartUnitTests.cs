﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueryPartUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System;
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="QueryPart"/> class.
    /// </summary>
    [TestFixture]
    public class QueryPartUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="QueryPart.CompareContent"/> method throws a <see cref="ArgumentNullException"/>
        /// when the specified <see cref="QueryPart"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void CompareContentWhenOtherNull()
        {
            var partMock = new Mock<QueryPart>(null, null) { CallBase = true };

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => partMock.Object.CompareContent(null));
            Assert.That(exception.ParamName, Is.EqualTo("otherPart"));
        }

        /// <summary>
        /// Tests if the <see cref="QueryPart.CompareContent"/> method returns <c>true</c> when the content string of
        /// the specified part is equal to the content string of the current part.
        /// </summary>
        [Test]
        public void CompareContentWhenContentStringEqual()
        {
            var part1Mock = new Mock<QueryPart>(null, null) { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("Str");

            var part2Mock = new Mock<QueryPart>(null, null) { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("Str");

            bool result = part1Mock.Object.CompareContent(part2Mock.Object);

            Assert.That(result, Is.True);
            part1Mock.VerifyAll();
            part2Mock.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="QueryPart.CompareContent"/> method returns <c>false</c> when the content string of
        /// the specified part is not equal to the content string of the current part.
        /// </summary>
        [Test]
        public void CompareContentWhenContentStringNotEqual()
        {
            var part1Mock = new Mock<QueryPart>(null, null) { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("Str1");

            var part2Mock = new Mock<QueryPart>(null, null) { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("Str2");

            bool result = part1Mock.Object.CompareContent(part2Mock.Object);

            Assert.That(result, Is.False);
            part1Mock.VerifyAll();
            part2Mock.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="QueryPart.ToString"/> method works properly when the parent query part is
        /// <c>null</c>.
        /// </summary>
        /// <param name="name">The name of the query part.</param>
        /// <param name="content">The content string of the query part.</param>
        /// <param name="expected">The expected result.</param>
        [TestCase("Name", "Content", "[Name|Content]")]
        [TestCase(null, "Content", "[Content]")]
        [TestCase("", "Content", "[Content]")]
        [TestCase("Name", null, "[Name]")]
        [TestCase("Name", "", "[Name]")]
        public void ToStringWhenParentNull(string name, string content, string expected)
        {
            var partMock = new Mock<QueryPart>(null, name) { CallBase = true };
            partMock.Setup(x => x.GetContentString()).Returns(content);

            string result = partMock.Object.ToString();

            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests if the <see cref="QueryPart.ToString"/> method works properly when the parent query part is not
        /// <c>null</c>.
        /// </summary>
        /// <param name="name">The name of the query part.</param>
        /// <param name="content">The content string of the query part.</param>
        /// <param name="expected">The expected result.</param>
        [TestCase("Name", "Content", "[Name|Content]->parent")]
        [TestCase(null, "Content", "[Content]->parent")]
        [TestCase("", "Content", "[Content]->parent")]
        [TestCase("Name", null, "[Name]->parent")]
        [TestCase("Name", "", "[Name]->parent")]
        public void ToStringWhenParentNotNull(string name, string content, string expected)
        {
            var parentMock = new Mock<QueryPart>(null, null);
            parentMock.Setup(x => x.ToString()).Returns("parent");

            var partMock = new Mock<QueryPart>(parentMock.Object, name) { CallBase = true };
            partMock.Setup(x => x.GetContentString()).Returns(content);

            string result = partMock.Object.ToString();

            Assert.That(result, Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.Equals(object)"/> method when the other query part is equal to the current
        /// query part.
        /// </summary>
        [Test]
        public void EqualsWhenEqual()
        {
            var part1Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content");

            var part2Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("content");

            Assert.That(part1Mock.Object.Equals(part2Mock.Object), Is.True);
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.Equals(object)"/> method when the other query part is <c>null</c>.
        /// </summary>
        [Test]
        public void EqualsWhenArgumentNull()
        {
            var part1Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content");

            Assert.That(part1Mock.Object.Equals(null), Is.False);
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.Equals(object)"/> method when the parent of the other query part is not
        /// equal to the parent of the current query part.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentParent()
        {
            QueryPart parent1Mock = new Mock<QueryPart>(null, null).Object;
            var part1Mock = new Mock<QueryPart>(parent1Mock, "name") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content");

            QueryPart parent2Mock = new Mock<QueryPart>(null, null).Object;
            var part2Mock = new Mock<QueryPart>(parent2Mock, "name") { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("content");

            // NOTE: We don't consider the parent when comparing query parts, so this should return true!
            Assert.That(part1Mock.Object.Equals(part2Mock.Object), Is.True);
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.Equals(object,bool)"/> method when the parent of the other query part is
        /// not equal to the parent of the current query part.
        /// </summary>
        [Test]
        public void EqualsRecursiveWhenDifferentParent()
        {
            QueryPart parent1Mock = new Mock<QueryPart>(null, null).Object;
            var part1Mock = new Mock<QueryPart>(parent1Mock, "name") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content");

            QueryPart parent2Mock = new Mock<QueryPart>(null, null).Object;
            var part2Mock = new Mock<QueryPart>(parent2Mock, "name") { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("content");

            Assert.That(part1Mock.Object.Equals(part2Mock.Object, true), Is.False);
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.Equals(object)"/> method when the type of the other query part is not equal
        /// to the type of the current query part.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentPartType()
        {
            var part1Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content");

            var part2 = new SwitchQueryPart(null, "name", true);

            Assert.That(part1Mock.Object.Equals(part2), Is.False);
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.Equals(object)"/> method when the name of the other query part is not equal
        /// to the name of the current query part.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentPartName()
        {
            var part1Mock = new Mock<QueryPart>(null, "name1") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content");

            var part2Mock = new Mock<QueryPart>(null, "name2") { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("content");

            Assert.That(part1Mock.Object.Equals(part2Mock.Object), Is.False);
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.Equals(object)"/> method when the content of the other query part is not
        /// equal to the content of the current query part.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentPartContent()
        {
            var part1Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content1");

            var part2Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("content2");

            Assert.That(part1Mock.Object.Equals(part2Mock.Object), Is.False);
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.GetHashCode"/> method when the query parts are equal.
        /// </summary>
        [Test]
        public void GetHashCodeWhenEqual()
        {
            var part1Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content");

            var part2Mock = new Mock<QueryPart>(null, "name") { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("content");

            Assert.That(part1Mock.Object.GetHashCode(), Is.EqualTo(part2Mock.Object.GetHashCode()));
        }

        /// <summary>
        /// Tests the <see cref="QueryPart.GetHashCode"/> method when the query parts are not equal.
        /// </summary>
        [Test]
        public void GetHashCodeWhenNotEqual()
        {
            var part1Mock = new Mock<QueryPart>(null, "name1") { CallBase = true };
            part1Mock.Setup(x => x.GetContentString()).Returns("content1");

            var part2Mock = new Mock<QueryPart>(null, "name2") { CallBase = true };
            part2Mock.Setup(x => x.GetContentString()).Returns("content2");

            Assert.That(part1Mock.Object.GetHashCode(), Is.Not.EqualTo(part2Mock.Object.GetHashCode()));
        }
    }
}
