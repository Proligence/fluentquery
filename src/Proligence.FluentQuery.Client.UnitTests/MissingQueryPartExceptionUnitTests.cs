﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MissingQueryPartExceptionUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.UnitTests
{
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="MissingQueryPartException"/> class.
    /// </summary>
    [TestFixture]
    public class MissingQueryPartExceptionUnitTests
    {
        /// <summary>
        /// Tests the default constructor.
        /// </summary>
        [Test]
        public void TestDefaultConstructor()
        {
            var exception = new MissingQueryPartException();

            Assert.That(exception.QueryPartName, Is.Null);
            Assert.That(exception.QueryPartType, Is.Null);
        }

        /// <summary>
        /// Tests the constructor which takes a query part type and name.
        /// </summary>
        [Test]
        public void TestConstructorWithQueryPartTypeAndName()
        {
            var exception = new MissingQueryPartException(typeof(SwitchQueryPart), "Test");

            Assert.That(exception.QueryPartType, Is.EqualTo(typeof(SwitchQueryPart)));
            Assert.That(exception.QueryPartName, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests the default exception message when only the query part type is specified.
        /// </summary>
        [Test]
        public void TestDefaultMessageWhenQueryPartTypeSpecified()
        {
            var exception = new MissingQueryPartException(typeof(SwitchQueryPart), null);

            Assert.That(exception.Message, Is.EqualTo("Failed to find query part of type 'SwitchQueryPart'."));
        }

        /// <summary>
        /// Tests the default exception message when only the query part name is specified.
        /// </summary>
        [Test]
        public void TestDefaultMessageWhenQueryPartNameSpecified()
        {
            var exception = new MissingQueryPartException(null, "Test");

            Assert.That(exception.Message, Is.EqualTo("Failed to find query part with name 'Test'."));
        }

        /// <summary>
        /// Tests the default exception message when the query part type and name is specified.
        /// </summary>
        [Test]
        public void TestDefaultMessageWhenQueryPartTypeAndNameSpecified()
        {
            var exception = new MissingQueryPartException(typeof(SwitchQueryPart), "Test");

            Assert.That(
                exception.Message,
                Is.EqualTo("Failed to find query part with name 'Test' of type 'SwitchQueryPart'."));
        }

        /// <summary>
        /// Tests the constructor which takes an exception message.
        /// </summary>
        [Test]
        public void TestConstructorWithMessage()
        {
            var exception = new MissingQueryPartException("My message.");

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.QueryPartType, Is.Null);
            Assert.That(exception.QueryPartName, Is.Null);
        }

        /// <summary>
        /// Tests the constructor which takes a message, query part type and name.
        /// </summary>
        [Test]
        public void TestConstructorWithMessageAndQueryPartData()
        {
            var exception = new MissingQueryPartException("My message.", typeof(SwitchQueryPart), "Test");

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.QueryPartType, Is.EqualTo(typeof(SwitchQueryPart)));
            Assert.That(exception.QueryPartName, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests the constructor which takes a message and inner exception.
        /// </summary>
        [Test]
        public void TestConstructorWithMessageAndInnerException()
        {
            var innerException = new InvalidOperationException();
            var exception = new MissingQueryPartException("My message.", innerException);

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.InnerException, Is.EqualTo(innerException));
            Assert.That(exception.QueryPartType, Is.Null);
            Assert.That(exception.QueryPartName, Is.Null);
        }

        /// <summary>
        /// Tests the constructor which takes a message, inner exception, query part type and name.
        /// </summary>
        [Test]
        public void TestConstructorWithMessageInnerExceptionAndQueryPartData()
        {
            var innerException = new InvalidOperationException();
            var exception = new MissingQueryPartException(
                "My message.", innerException, typeof(SwitchQueryPart), "Test");

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.InnerException, Is.EqualTo(innerException));
            Assert.That(exception.QueryPartType, Is.EqualTo(typeof(SwitchQueryPart)));
            Assert.That(exception.QueryPartName, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if exception objects are serialized correctly.
        /// </summary>
        [Test]
        public void TestExceptionSerialization()
        {
            var exception = new MissingQueryPartException("My message.", typeof(SwitchQueryPart), "Test");

            MissingQueryPartException deserializedException;
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();

                formatter.Serialize(stream, exception);
                stream.Flush();
                stream.Seek(0L, SeekOrigin.Begin);

                deserializedException = (MissingQueryPartException)formatter.Deserialize(stream);
            }

            Assert.That(deserializedException.Message, Is.EqualTo("My message."));
            Assert.That(deserializedException.QueryPartType, Is.EqualTo(typeof(SwitchQueryPart)));
            Assert.That(deserializedException.QueryPartName, Is.EqualTo("Test"));
        }
    }
}