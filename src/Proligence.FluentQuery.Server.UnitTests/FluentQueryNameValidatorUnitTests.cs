﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryNameValidatorUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Runtime.Serialization;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="FluentQueryNameValidator"/> class.
    /// </summary>
    [TestFixture]
    public class FluentQueryNameValidatorUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator"/> throws a <see cref="ArgumentNullException"/> when the
        /// specified query is <c>null</c>.
        /// </summary>
        [Test]
        public void ValidateWhenQueryNull()
        {
            var validator = new FluentQueryNameValidator();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => validator.Validate(null));
            Assert.That(exception.ParamName, Is.EqualTo("queryRootPart"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator"/> does not throw any exception when the specified query
        /// is valid.
        /// </summary>
        [Test]
        public void ValidateWhenQueryValid()
        {
            var validator = new FluentQueryNameValidator(new[] { "Name1", "Name2" });
            var query = new TestQueryPart(null, "Name1").Test("Name2");

            Assert.DoesNotThrow(
                () => validator.Validate(query));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator"/> throws a <see cref="QueryPartException"/> when the
        /// specified query is not valid.
        /// </summary>
        [Test]
        public void ValidateWhenQueryInvalid()
        {
            var validator = new FluentQueryNameValidator(new[] { "Name1", "Name2" });
            var query = new TestQueryPart(null, "Name1").Test("Name3");

            QueryPartException exception = Assert.Throws<QueryPartException>(
                () => validator.Validate(query));

            Assert.That(exception.QueryPartName, Is.EqualTo("Name3"));
            Assert.That(exception.QueryPartType, Is.EqualTo(typeof(TestQueryPart)));
            Assert.That(
                exception.Message,
                Is.EqualTo("The fluent query contains an invalid or unsupported query part 'Name3'."));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator"/> does not validate query parts which do not have a name.
        /// </summary>
        [Test]
        public void ValidateDoesNotValidateUnnamedQueryParts()
        {
            var validator = new FluentQueryNameValidator(new[] { "Name1", "Name2" });
            var query = new TestQueryPart(null, "Name1").Test(null).Test("Name2");

            Assert.DoesNotThrow(
                () => validator.Validate(query));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator"/> works correctly when query part names are added using
        /// the <see cref="FluentQueryNameValidator.AddQueryPartName"/> method.
        /// </summary>
        [Test]
        public void AddQueryPartNameAndValidate()
        {
            var validator = new FluentQueryNameValidator();
            validator.AddQueryPartName("Name1");
            validator.AddQueryPartName("Name2");

            var query = new TestQueryPart(null, "Name1").Test("Name3");

            Assert.Throws<QueryPartException>(() => validator.Validate(query));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator.AddQueryPartName"/> method works correctly when the
        /// specified name is not added to the validator.
        /// </summary>
        [Test]
        public void AddQueryPartNameWhenNameNotAdded()
        {
            var validator = new FluentQueryNameValidatorStub();

            validator.AddQueryPartName("Name1");
            validator.AddQueryPartName("Name2");
            validator.AddQueryPartName("Name3");

            Assert.That(validator.ValidNames.ToArray(), Is.EquivalentTo(new[] { "Name1", "Name2", "Name3" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator.AddQueryPartName"/> method works correctly when the
        /// specified name is already added to the validator.
        /// </summary>
        [Test]
        public void AddQueryPartNameWhenNameAlreadyAdded()
        {
            var validator = new FluentQueryNameValidatorStub();

            validator.AddQueryPartName("Name1");
            validator.AddQueryPartName("Name1");
            validator.AddQueryPartName("Name3");

            Assert.That(validator.ValidNames.ToArray(), Is.EquivalentTo(new[] { "Name1", "Name3" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator.AddQueryPartName"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified name is <c>null</c>.
        /// </summary>
        [Test]
        public void AddQueryPartNameWhenNameNull()
        {
            var validator = new FluentQueryNameValidator();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => validator.AddQueryPartName(null));
            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator.AddQueryPartNames"/> method works correctly when the
        /// specified names are not added to the validator.
        /// </summary>
        [Test]
        public void AddQueryPartNamesWhenNamesNotAdded()
        {
            var validator = new FluentQueryNameValidatorStub();

            validator.AddQueryPartNames(new[] { "Name1", "Name2" });
            validator.AddQueryPartNames(new[] { "Name3", "Name4" });
            
            Assert.That(validator.ValidNames.ToArray(), Is.EquivalentTo(new[] { "Name1", "Name2", "Name3", "Name4" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator.AddQueryPartNames"/> method works correctly when one of
        /// the specified names is already added to the validator.
        /// </summary>
        [Test]
        public void AddQueryPartNamesWhenNameAlreadyAdded()
        {
            var validator = new FluentQueryNameValidatorStub();

            validator.AddQueryPartNames(new[] { "Name1", "Name1", "Name2" });

            Assert.That(validator.ValidNames.ToArray(), Is.EquivalentTo(new[] { "Name1", "Name2" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator.AddQueryPartName"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified names collection is <c>null</c>.
        /// </summary>
        [Test]
        public void AddQueryPartNamesWhenNamesNull()
        {
            var validator = new FluentQueryNameValidator();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => validator.AddQueryPartNames(null));
            Assert.That(exception.ParamName, Is.EqualTo("names"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryNameValidator.AddQueryPartName"/> method throws a
        /// <see cref="ArgumentException"/> when the specified names collection contains a <c>null</c> value.
        /// </summary>
        [Test]
        public void AddQueryPartNamesWhenNamesContainsNull()
        {
            var validator = new FluentQueryNameValidator();

            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => validator.AddQueryPartNames(new[] { "Test1", null, "Test2" }));

            Assert.That(exception.ParamName, Is.EqualTo("names"));
            Assert.That(
                exception.Message, 
                Is.StringContaining("The specified names collection contains a null value."));
        }

        /// <summary>
        /// Implements a subclass of <see cref="FluentQueryNameValidator"/> for testing purposes.
        /// </summary>
        private class FluentQueryNameValidatorStub : FluentQueryNameValidator
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="FluentQueryNameValidatorStub"/> class.
            /// </summary>
            public FluentQueryNameValidatorStub()
            {
            }

            /// <summary>
            /// Gets the list of valid query part names.
            /// </summary>
            public new IList<string> ValidNames
            {
                get
                {
                    return base.ValidNames;
                }
            }
        }

        /// <summary>
        /// Test query part.
        /// </summary>
        [DataContract]
        private class TestQueryPart : QueryPart
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestQueryPart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            public TestQueryPart(QueryPart parentPart, string name)
                : base(parentPart, name)
            {
            }

            /// <summary>
            /// Test query method.
            /// </summary>
            /// <param name="name">The name of the new query part.</param>
            /// <returns>The fluent query.</returns>
            public TestQueryPart Test(string name)
            {
                return new TestQueryPart(this, name);
            }

            /// <summary>
            /// Returns a <see cref="string"/> which represents the contents of the query part.
            /// </summary>
            /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
            [ExcludeFromCodeCoverage]
            public override string GetContentString()
            {
                throw new NotImplementedException();
            }
        }
    }
}