﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryReaderBaseExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server.UnitTests
{
    using System;
    using System.Collections.Generic;
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="FluentQueryReaderBaseExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class FluentQueryReaderBaseExtensionsUnitTests
    {
        /// <summary>
        /// The fluent query reader mock.
        /// </summary>
        private Mock<FluentQueryReaderBase> readerMock;

        /// <summary>
        /// The fluent query reader instance.
        /// </summary>
        private FluentQueryReaderBase reader;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.readerMock = new Mock<FluentQueryReaderBase>(new SwitchQueryPart(null, "Dummy", false));
            this.reader = this.readerMock.Object;
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetSwitch"/> method when the specified fluent query
        /// reader is <c>null</c>.
        /// </summary>
        [Test]
        public void GetSwitchWhenReaderNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                FluentQueryReaderBaseExtensions.GetSwitch(null, "Test"));

            Assert.That(exception.ParamName, Is.EqualTo("reader"));
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetSwitch"/> method when the specified fluent query
        /// part name is <c>null</c>.
        /// </summary>
        [Test]
        public void GetSwitchWhenNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetSwitch(null));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetSwitch"/> method when the specified fluent query
        /// part name is an empty string.
        /// </summary>
        [Test]
        public void GetSwitchWhenNameEmpty()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetSwitch(string.Empty));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetSwitch"/> method when the specified parameters are
        /// valid.
        /// </summary>
        /// <param name="enabled">Specifies if the tested switch is enabled.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void GetSwitchWhenParametersValid(bool enabled)
        {
            this.readerMock.Setup(x => x.GetSwitch<SwitchQueryPart>("Test")).Returns(enabled);

            bool result = this.reader.GetSwitch("Test");

            Assert.That(result, Is.EqualTo(enabled));
            this.readerMock.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetValue{TValue}"/> method when the specified
        /// fluent query reader is <c>null</c>.
        /// </summary>
        [Test]
        public void GetValueWhenReaderNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                FluentQueryReaderBaseExtensions.GetValue<string>(null, "Test"));

            Assert.That(exception.ParamName, Is.EqualTo("reader"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetValue{TValue}"/> method when the specified
        /// query part name is <c>null</c>.
        /// </summary>
        [Test]
        public void GetValueWhenNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetValue<string>(null));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetValue{TValue}"/> method when the specified
        /// query part name is an empty string.
        /// </summary>
        [Test]
        public void GetValueWhenNameEmpty()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetValue<string>(string.Empty));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetValue{TValue}"/> method when the specified
        /// parameters are valid.
        /// </summary>
        [Test]
        public void GetValueWhenParametersValid()
        {
            this.readerMock.Setup(x => x.GetValue<ValueQueryPart<string>, string>("Test")).Returns("Result");

            string result = this.reader.GetValue<string>("Test");

            Assert.That(result, Is.EqualTo("Result"));
            this.readerMock.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetRequiredValue{TValue}"/> method when the
        /// specified fluent query reader is <c>null</c>.
        /// </summary>
        [Test]
        public void GetRequiredValueWhenReaderNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                FluentQueryReaderBaseExtensions.GetRequiredValue<string>(null, "Test"));

            Assert.That(exception.ParamName, Is.EqualTo("reader"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetRequiredValue{TValue}"/> method when the
        /// specified query part name is <c>null</c>.
        /// </summary>
        [Test]
        public void GetRequiredValueWhenNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetRequiredValue<string>(null));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetRequiredValue{TValue}"/> method when the
        /// specified query part name is an empty string.
        /// </summary>
        [Test]
        public void GetRequiredValueWhenNameEmpty()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetRequiredValue<string>(string.Empty));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetRequiredValue{TValue}"/> method when a value query
        /// part with the specified name exists in the query.
        /// </summary>
        [Test]
        public void GetRequiredValueWhenQueryPartExists()
        {
            var queryPart = new StringQueryPart(null, "Test", "Result");
            this.readerMock.Setup(x => x.GetQueryPart<ValueQueryPart<string>>("Test")).Returns(queryPart);

            string result = this.reader.GetRequiredValue<string>("Test");

            Assert.That(result, Is.EqualTo("Result"));
            this.readerMock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetRequiredValue{TValue}"/> method when a value query
        /// part with the specified name does not exist in the query.
        /// </summary>
        [Test]
        public void GetRequiredValueWhenQueryPartDoesNotExist()
        {
            this.readerMock
                .Setup(x => x.GetQueryPart<ValueQueryPart<string>>("Test"))
                .Returns((ValueQueryPart<string>)null);

            MissingQueryPartException exception = Assert.Throws<MissingQueryPartException>(
                () => this.reader.GetRequiredValue<string>("Test"));

            Assert.That(exception.QueryPartName, Is.EqualTo("Test"));
            Assert.That(exception.QueryPartType, Is.EqualTo(typeof(ValueQueryPart<string>)));
            this.readerMock.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetList{TValue}"/> method when the specified
        /// fluent query reader is <c>null</c>.
        /// </summary>
        [Test]
        public void GetListWhenReaderNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                FluentQueryReaderBaseExtensions.GetList<string>(null, "Test"));

            Assert.That(exception.ParamName, Is.EqualTo("reader"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetList{TValue}"/> method when the specified
        /// query part name is <c>null</c>.
        /// </summary>
        [Test]
        public void GetListWhenNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetList<string>(null));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetList{TValue}"/> method when the specified
        /// query part name is an empty string.
        /// </summary>
        [Test]
        public void GetListWhenNameEmpty()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetList<string>(string.Empty));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetList{TValue}"/> method when the specified
        /// parameters are valid.
        /// </summary>
        [Test]
        public void GetListWhenParametersValid()
        {
            var items = new[] { "one", "two", "three" };
            this.readerMock.Setup(x => x.GetList<ListQueryPart<string>, string>("Test")).Returns(items);

            IEnumerable<string> result = this.reader.GetList<string>("Test");

            Assert.That(result, Is.SameAs(items));
            this.readerMock.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetList{TValue}"/> method when the specified
        /// fluent query reader is <c>null</c>.
        /// </summary>
        [Test]
        public void GetRequiredListWhenReaderNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                FluentQueryReaderBaseExtensions.GetRequiredList<string>(null, "Test"));

            Assert.That(exception.ParamName, Is.EqualTo("reader"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetRequiredList{TValue}"/> method when the
        /// specified query part name is <c>null</c>.
        /// </summary>
        [Test]
        public void GetRequiredListWhenNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetRequiredList<string>(null));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBaseExtensions.GetRequiredList{TValue}"/> method when the
        /// specified query part name is an empty string.
        /// </summary>
        [Test]
        public void GetRequiredListWhenNameEmpty()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() =>
                this.reader.GetRequiredList<string>(string.Empty));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetRequiredList{TValue}"/> method when a list query
        /// part with the specified name exists in the query.
        /// </summary>
        [Test]
        public void GetRequiredListWhenQueryPartExists()
        {
            var items = new[] { "one", "two", "three" };
            var queryPart = new StringListQueryPart(null, "Test", items);
            this.readerMock.Setup(x => x.GetQueryPart<ListQueryPart<string>>("Test")).Returns(queryPart);

            IEnumerable<string> result = this.reader.GetRequiredList<string>("Test");

            Assert.That(result, Is.SameAs(items));
            this.readerMock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="FluentQueryReaderBaseExtensions.GetRequiredList{TValue}"/> method when a list query
        /// part with the specified name does not exist in the query.
        /// </summary>
        [Test]
        public void GetRequiredListWhenQueryPartDoesNotExist()
        {
            this.readerMock
                .Setup(x => x.GetQueryPart<ListQueryPart<string>>("Test"))
                .Returns((ListQueryPart<string>)null);

            MissingQueryPartException exception = Assert.Throws<MissingQueryPartException>(
                () => this.reader.GetRequiredList<string>("Test"));

            Assert.That(exception.QueryPartName, Is.EqualTo("Test"));
            Assert.That(exception.QueryPartType, Is.EqualTo(typeof(ListQueryPart<string>)));
            this.readerMock.VerifyAll();
        }
    }
}