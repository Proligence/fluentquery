﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryReaderUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements unit tests for the <see cref="FluentQueryReader"/> class.
    /// </summary>
    [TestFixture]
    public class FluentQueryReaderUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="FluentQueryReader"/> class correctly initializes a new instance
        /// when the specified root query part is not <c>null</c>.
        /// </summary>
        [Test]
        public void CreateReaderWhenRootQueryPartNotNull()
        {
            QueryPart rootPart = new Mock<QueryPart>(null, null).Object;
            
            FluentQueryReader reader = new FluentQueryReader(rootPart);

            Assert.That(reader.QueryRootPart, Is.SameAs(rootPart));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="FluentQueryReader"/> class throws a
        /// <see cref="ArgumentNullException"/> when the specified root query part is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateReaderWhenRootQueryPartNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new FluentQueryReader(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("queryRootPart"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryPart{TPart}"/> method works properly if no part name is
        /// specified and there is only one part with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartWhenNoNameAndSingleQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            TestQueryPart2 result = reader.GetQueryPart<TestQueryPart2>();

            Assert.That(result, Is.SameAs(part2));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryPart{TPart}"/> method works properly if no part name is
        /// specified and there are many parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartWhenNoNameAndManyQueryParts()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            TestQueryPart result = reader.GetQueryPart<TestQueryPart>();

            Assert.That(result, Is.SameAs(rootPart));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryPart{TPart}"/> method works properly if no part name is
        /// specified and there are no parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartWhenNoNameAndNoQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            TestQueryPart2 result = reader.GetQueryPart<TestQueryPart2>();

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryPart{TPart}"/> method works properly if a part name is
        /// specified and there is only one part which satisfies the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartWhenNameSpecifiedAndSingleQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            TestQueryPart result = reader.GetQueryPart<TestQueryPart>("Part3");

            Assert.That(result, Is.SameAs(part3));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryPart{TPart}"/> method works properly if a part name is
        /// specified and there are many parts which satisfies the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartWhenNameSpecifiedAndManyQueryParts()
        {
            var part3 = new TestQueryPart(null, "Part1");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            TestQueryPart result = reader.GetQueryPart<TestQueryPart>("Part1");

            Assert.That(result, Is.SameAs(rootPart));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryPart{TPart}"/> method works properly if a part name is
        /// specified and there are many parts which satisfy the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartWhenNameSpecifiedAndNoQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            TestQueryPart result = reader.GetQueryPart<TestQueryPart>("Part4");

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryParts{TPart}"/> method works properly if no part name is
        /// specified and there is only one part with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartsWhenNoNameAndSingleQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<TestQueryPart2> result = reader.GetQueryParts<TestQueryPart2>();

            Assert.That(result.ToArray(), Is.EqualTo(new[] { part2 }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryParts{TPart}"/> method works properly if no part name is
        /// specified and there are many parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartsWhenNoNameAndManyQueryParts()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<TestQueryPart> result = reader.GetQueryParts<TestQueryPart>();

            Assert.That(result.ToArray(), Is.EqualTo(new[] { rootPart, part3 }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryParts{TPart}"/> method works properly if no part name is
        /// specified and there are no parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartsWhenNoNameAndNoQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<TestQueryPart2> result = reader.GetQueryParts<TestQueryPart2>();

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryParts{TPart}"/> method works properly if a part name is
        /// specified and there is only one part which satisfies the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartsWhenNameSpecifiedAndSingleQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<TestQueryPart> result = reader.GetQueryParts<TestQueryPart>("Part3");

            Assert.That(result.ToArray(), Is.EqualTo(new[] { part3 }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryParts{TPart}"/> method works properly if a part name is
        /// specified and there are many parts which satisfies the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartsWhenNameSpecifiedAndManyQueryParts()
        {
            var part3 = new TestQueryPart(null, "Part1");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<TestQueryPart> result = reader.GetQueryParts<TestQueryPart>("Part1");

            Assert.That(result.ToArray(), Is.EqualTo(new[] { rootPart, part3 }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReader.GetQueryParts{TPart}"/> method works properly if a part name is
        /// specified and there are many parts which satisfy the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetQueryPartsWhenNameSpecifiedAndNoQueryPart()
        {
            var part3 = new TestQueryPart(null, "Part3");
            var part2 = new TestQueryPart2(part3, "Part2");
            var rootPart = new TestQueryPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<TestQueryPart> result = reader.GetQueryParts<TestQueryPart>("Part4");

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetSwitch{TPart}"/> method works properly if no part name is
        /// specified and there is a single query part with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetSwitchWhenNoNameAndSingleQueryPart()
        {
            var part3 = new TestSwitchPart(null, "Part3");
            var part2 = new TestSwitchPart2(part3, "Part2", true);
            var rootPart = new TestSwitchPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            bool result = reader.GetSwitch<TestSwitchPart2>();

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetSwitch{TPart}"/> method works properly if no part name is
        /// specified and there are many query parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetSwitchWhenNoNameAndManyQueryParts()
        {
            var part3 = new TestSwitchPart(null, "Part3");
            var part2 = new TestSwitchPart2(part3, "Part2");
            var rootPart = new TestSwitchPart(part2, "Part1", true);

            var reader = new FluentQueryReader(rootPart);

            bool result = reader.GetSwitch<TestSwitchPart>();

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetSwitch{TPart}"/> method works properly if no part name is
        /// specified and there is no query part with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetSwitchWhenNoNameAndNoQueryPart()
        {
            var part3 = new TestSwitchPart(null, "Part3");
            var part2 = new TestSwitchPart(part3, "Part2");
            var rootPart = new TestSwitchPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            bool result = reader.GetSwitch<TestSwitchPart2>();

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetSwitch{TPart}"/> method works properly if a part name is
        /// specified and there is a single query part which satisfies the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetSwitchWhenNameSpecifiedAndSingleQueryPart()
        {
            var part3 = new TestSwitchPart(null, "Part3", true);
            var part2 = new TestSwitchPart2(part3, "Part2");
            var rootPart = new TestSwitchPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            bool result = reader.GetSwitch<TestSwitchPart>("Part3");

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetSwitch{TPart}"/> method works properly if a part name is
        /// specified and there are many query parts which satisfy the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetSwitchWhenNameSpecifiedAndManyQueryParts()
        {
            var part3 = new TestSwitchPart(null, "Part1");
            var part2 = new TestSwitchPart2(part3, "Part2");
            var rootPart = new TestSwitchPart(part2, "Part1", true);

            var reader = new FluentQueryReader(rootPart);

            bool result = reader.GetSwitch<TestSwitchPart>("Part1");

            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetSwitch{TPart}"/> method works properly if a part name is
        /// specified and there is no query part which satisfies the specified type and name in the fluent query.
        /// </summary>
        [Test]
        public void GetSwitchWhenNameSpecifiedAndNoQueryPart()
        {
            var part3 = new TestSwitchPart(null, "Part3");
            var part2 = new TestSwitchPart2(part3, "Part2");
            var rootPart = new TestSwitchPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            bool result = reader.GetSwitch<TestSwitchPart>("Part4");

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetValue{TPart,TValue}"/> method works properly if no part
        /// name is specified and there is a single query part with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetValueWhenNoNameAndSingleQueryPart()
        {
            var part3 = new TestValuePart(null, "Part3");
            var part2 = new TestValuePart2(part3, "Part2", "Test");
            var rootPart = new TestValuePart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            string result = reader.GetValue<TestValuePart2, string>();

            Assert.That(result, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetValue{TPart,TValue}"/> method works properly if no part
        /// name is specified and there are many query parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetValueWhenNoNameAndManyQueryParts()
        {
            var part3 = new TestValuePart(null, "Part3");
            var part2 = new TestValuePart2(part3, "Part2");
            var rootPart = new TestValuePart(part2, "Part1", "Test");

            var reader = new FluentQueryReader(rootPart);

            string result = reader.GetValue<TestValuePart, string>();

            Assert.That(result, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetValue{TPart,TValue}"/> method works properly if no part
        /// name is specified and there is no query part with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetValueWhenNoNameAndNoQueryPart()
        {
            var part3 = new TestValuePart(null, "Part3");
            var part2 = new TestValuePart(part3, "Part2");
            var rootPart = new TestValuePart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            string result = reader.GetValue<TestValuePart2, string>();

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetValue{TPart,TValue}"/> method works properly if a part
        /// name is specified and there is a single query part with the specified name and type in the fluent query.
        /// </summary>
        [Test]
        public void GetValueWhenNameSpecifiedAndSingleQueryPart()
        {
            var part3 = new TestValuePart(null, "Part3", "Test");
            var part2 = new TestValuePart2(part3, "Part2");
            var rootPart = new TestValuePart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            string result = reader.GetValue<TestValuePart, string>("Part3");

            Assert.That(result, Is.SameAs("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetValue{TPart,TValue}"/> method works properly if a part
        /// name is specified and there are many query parts with the specified name and type in the fluent query.
        /// </summary>
        [Test]
        public void GetValueWhenNameSpecifiedAndManyQueryParts()
        {
            var part3 = new TestValuePart(null, "Part1");
            var part2 = new TestValuePart2(part3, "Part2");
            var rootPart = new TestValuePart(part2, "Part1", "Test");

            var reader = new FluentQueryReader(rootPart);

            string result = reader.GetValue<TestValuePart, string>("Part1");

            Assert.That(result, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetValue{TPart,TValue}"/> method works properly if a part
        /// name is specified and there is no query part with the specified name and type in the fluent query.
        /// </summary>
        [Test]
        public void GetValueWhenNameSpecifiedAndNoQueryPart()
        {
            var part3 = new TestValuePart(null, "Part3");
            var part2 = new TestValuePart2(part3, "Part2");
            var rootPart = new TestValuePart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            string result = reader.GetValue<TestValuePart, string>("Part4");

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetList{TPart,TItem}"/> method works properly if no part
        /// name is specified and there is a single query part with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetListWhenNoNameAndSingleQueryPart()
        {
            var part3 = new TestListPart(null, "Part3");
            var part2 = new TestListPart2(part3, "Part2", "One", "Two", "Three");
            var rootPart = new TestListPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<string> result = reader.GetList<TestListPart2, string>();

            Assert.That(result.ToArray(), Is.EqualTo(new[] { "One", "Two", "Three" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetList{TPart,TItem}"/> method works properly if no part
        /// name is specified and there are many query parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetListWhenNoNameAndAndManyQueryPart()
        {
            var part3 = new TestListPart(null, "Part3");
            var part2 = new TestListPart2(part3, "Part2");
            var rootPart = new TestListPart(part2, "Part1", "One", "Two", "Three");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<string> result = reader.GetList<TestListPart, string>();

            Assert.That(result.ToArray(), Is.EqualTo(new[] { "One", "Two", "Three" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetList{TPart,TItem}"/> method works properly if no part
        /// name is specified and there is no query parts with the specified type in the fluent query.
        /// </summary>
        [Test]
        public void GetListWhenNoNameAndAndNoQueryPart()
        {
            var part3 = new TestListPart(null, "Part3");
            var part2 = new TestListPart(part3, "Part2");
            var rootPart = new TestListPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<string> result = reader.GetList<TestListPart2, string>();

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetList{TPart,TItem}"/> method works properly if a part name
        /// is specified and there is a single query part with the specified name and type in the fluent query.
        /// </summary>
        [Test]
        public void GetListWhenNameSpecifiedAndSingleQueryPart()
        {
            var part3 = new TestListPart(null, "Part3", "One", "Two", "Three");
            var part2 = new TestListPart2(part3, "Part2");
            var rootPart = new TestListPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<string> result = reader.GetList<TestListPart, string>("Part3");

            Assert.That(result.ToArray(), Is.EqualTo(new[] { "One", "Two", "Three" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetList{TPart,TItem}"/> method works properly if a part name
        /// is specified and there are many query parts with the specified name and type in the fluent query.
        /// </summary>
        [Test]
        public void GetListWhenNameSpecifiedAndAndManyQueryPart()
        {
            var part3 = new TestListPart(null, "Part1");
            var part2 = new TestListPart2(part3, "Part2");
            var rootPart = new TestListPart(part2, "Part1", "One", "Two", "Three");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<string> result = reader.GetList<TestListPart, string>("Part1");

            Assert.That(result.ToArray(), Is.EqualTo(new[] { "One", "Two", "Three" }));
        }

        /// <summary>
        /// Tests if the <see cref="FluentQueryReaderBase.GetList{TPart,TItem}"/> method works properly if a part name
        /// is specified and there is no query part with the specified name and type in the fluent query.
        /// </summary>
        [Test]
        public void GetListWhenNameSpecifiedAndAndNoQueryPart()
        {
            var part3 = new TestListPart(null, "Part3");
            var part2 = new TestListPart2(part3, "Part2");
            var rootPart = new TestListPart(part2, "Part1");

            var reader = new FluentQueryReader(rootPart);

            IEnumerable<string> result = reader.GetList<TestListPart, string>("Part4");

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Test query part.
        /// </summary>
        private class TestQueryPart : QueryPart
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestQueryPart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            public TestQueryPart(QueryPart parentPart = null, string name = null)
                : base(parentPart, name)
            {
            }

            /// <summary>
            /// Returns a <see cref="string"/> which represents the contents of the query part.
            /// </summary>
            /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
            [ExcludeFromCodeCoverage]
            public override string GetContentString()
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Test query part.
        /// </summary>        
        private class TestQueryPart2 : QueryPart
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestQueryPart2"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            public TestQueryPart2(QueryPart parentPart = null, string name = null)
                : base(parentPart, name)
            {
            }

            /// <summary>
            /// Returns a <see cref="string"/> which represents the contents of the query part.
            /// </summary>
            /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
            [ExcludeFromCodeCoverage]
            public override string GetContentString()
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Test switch query part.
        /// </summary>
        private class TestSwitchPart : SwitchQueryPart
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestSwitchPart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="enabled">
            /// A value indicating whether the switch represented by this <see cref="SwitchQueryPart"/> is enabled.
            /// </param>
            public TestSwitchPart(QueryPart parentPart = null, string name = null, bool enabled = false)
                : base(parentPart, name, enabled)
            {
            }
        }

        /// <summary>
        /// Test switch query part.
        /// </summary>
        private class TestSwitchPart2 : SwitchQueryPart
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestSwitchPart2"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="enabled">
            /// A value indicating whether the switch represented by this <see cref="SwitchQueryPart"/> is enabled.
            /// </param>
            public TestSwitchPart2(QueryPart parentPart = null, string name = null, bool enabled = false)
                : base(parentPart, name, enabled)
            {
            }
        }

        /// <summary>
        /// Test value query part.
        /// </summary>
        private class TestValuePart : ValueQueryPart<string>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestValuePart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="value">The value to store in the query part.</param>
            public TestValuePart(QueryPart parentPart = null, string name = null, string value = null)
                : base(parentPart, name, value)
            {
            }
        }

        /// <summary>
        /// Test value query part.
        /// </summary>
        private class TestValuePart2 : ValueQueryPart<string>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestValuePart2"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="value">The value to store in the query part.</param>
            public TestValuePart2(QueryPart parentPart = null, string name = null, string value = null)
                : base(parentPart, name, value)
            {
            }
        }

        /// <summary>
        /// Test list query part.
        /// </summary>
        private class TestListPart : ListQueryPart<string>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestListPart"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="values">The values to store in the query part.</param>
            public TestListPart(QueryPart parentPart = null, string name = null, params string[] values)
                : base(parentPart, name, values)
            {
            }
        }

        /// <summary>
        /// Test list query part.
        /// </summary>
        private class TestListPart2 : ListQueryPart<string>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestListPart2"/> class.
            /// </summary>
            /// <param name="parentPart">The parent query part.</param>
            /// <param name="name">The name of the query part.</param>
            /// <param name="values">The values to store in the query part.</param>
            public TestListPart2(QueryPart parentPart = null, string name = null, params string[] values)
                : base(parentPart, name, values)
            {
            }
        }
    }
}