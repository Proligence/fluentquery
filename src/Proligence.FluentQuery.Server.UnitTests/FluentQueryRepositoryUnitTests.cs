﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryRepositoryUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Moq;
    using Moq.Protected;
    using global::NHibernate.Criterion;
    using NUnit.Framework;
    using Proligence.FluentQuery.Client.Contract;
    using Proligence.FluentQuery.Server.NHibernate;
    using Proligence.Helpers.NHibernate;

    /// <summary>
    /// Implements unit tests for the <see cref="FluentQueryRepository{TData, TEntity, TId}"/> class.
    /// </summary>
    [TestFixture]
    public class FluentQueryRepositoryUnitTests
    {
        /// <summary>
        /// Tests the <c>GetObjects</c> method when the specified fluent query is <c>null</c>.
        /// </summary>
        [Test]
        public void GetObjectsWhenQueryNull()
        {
            FluentQueryRepositoryStub repository = (new Mock<FluentQueryRepositoryStub> { CallBase = true }).Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                    () => repository.GetObjects(null));
            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests the <c>GetObjects</c> method in the common case.
        /// </summary>
        [Test]
        public void GetObjectsWhenCommonCase()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var query = new Mock<QueryPart>(null, null);

            var result = new[] { new TestData { Value = "V1" }, new TestData { Value = "V2" } };
            repository.Protected().Setup<IEnumerable<TestData>>("ExecuteGetQuery", query.Object).Returns(result);

            IEnumerable<TestEntity> actualResult = repository.Object.GetObjects(query.Object).ToArray();

            Assert.That(actualResult.Count(), Is.EqualTo(2));
            Assert.That(actualResult.ElementAt(0).Value, Is.EqualTo("V1"));
            Assert.That(actualResult.ElementAt(1).Value, Is.EqualTo("V2"));

            repository.VerifyAll();
        }

        /// <summary>
        /// Tests the <c>GetIds</c> method when the specified fluent query is <c>null</c>.
        /// </summary>
        [Test]
        public void GetIdsWhenQueryNull()
        {
            FluentQueryRepositoryStub repository = (new Mock<FluentQueryRepositoryStub> { CallBase = true }).Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.GetIds(null));
            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests the <c>GetIds</c> method in the common case.
        /// </summary>
        [Test]
        public void GetIdsWhenCommonCase()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var query = new Mock<QueryPart>(null, null);

            Guid id1 = Guid.NewGuid();
            Guid id2 = Guid.NewGuid();

            var result = new[]
            {
                new TestData { Id = id1, Value = "V1" },
                new TestData { Id = id2, Value = "V2" }
            };

            repository.Protected().Setup<IEnumerable<TestData>>("ExecuteGetQuery", query.Object).Returns(result);

            IEnumerable<Guid> actualResult = repository.Object.GetIds(query.Object).ToArray();

            Assert.That(actualResult.Count(), Is.EqualTo(2));
            Assert.That(actualResult.ElementAt(0), Is.EqualTo(id1));
            Assert.That(actualResult.ElementAt(1), Is.EqualTo(id2));

            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ExecuteGetQuery</c> method throws a <see cref="ArgumentNullException"/> when the specified
        /// fluent query is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteGetQueryWhenQueryNull()
        {
            FluentQueryRepositoryStub repository = (new Mock<FluentQueryRepositoryStub> { CallBase = true }).Object;
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.InvokeExecuteGetQuery(null));
            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests if the <c>ExecuteGetQuery</c> method validates the input fluent query using the validator returned by
        /// the <c>CreateGetQueryNameValidator</c> method.
        /// </summary>
        [Test]
        public void ExecuteGetQueryCheckIfQueryIsValidated()
        {
            var query = new Mock<QueryPart>(null, "TestPart");

            var validator = new Mock<FluentQueryNameValidator> { CallBase = true };
            validator.Protected().Setup<bool>("ValidateQueryPart", query.Object).Returns(false);
            
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            repository.Protected()
                .Setup<FluentQueryNameValidator>("CreateGetQueryNameValidator")
                .Returns(validator.Object);

            var exception = Assert.Throws<QueryPartException>(
                () => repository.Object.InvokeExecuteGetQuery(query.Object));

            Assert.That(
                exception.Message,
                Is.EqualTo("The fluent query contains an invalid or unsupported query part 'TestPart'."));

            validator.VerifyAll();
            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ExecuteGetQuery</c> method works correctly in the common case.
        /// </summary>
        [Test]
        public void ExecuteGetQueryWhenCommonCase()
        {
            var query = new Mock<QueryPart>(null, null);
            var criteria = new Mock<DetachedCriteria>(typeof(TestData));
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };

            repository.Protected()
                .Setup<DetachedCriteria>("BuildGetCriteria", ItExpr.IsAny<FluentQueryReader>())
                .Returns(criteria.Object);

            Guid id1 = Guid.NewGuid();
            Guid id2 = Guid.NewGuid();

            var result = new[]
            {
                new TestData { Id = id1, Value = "V1" },
                new TestData { Id = id2, Value = "V2" }
            };

            repository.Protected()
                .Setup<IEnumerable<TestData>>("ExecuteGetCriteria", criteria.Object, ItExpr.IsAny<FluentQueryReader>())
                .Returns(result);

            IEnumerable<TestData> actualResult = repository.Object.InvokeExecuteGetQuery(query.Object).ToArray();

            Assert.That(actualResult.Count(), Is.EqualTo(2));
            Assert.That(actualResult.ElementAt(0).Id, Is.EqualTo(id1));
            Assert.That(actualResult.ElementAt(1).Id, Is.EqualTo(id2));

            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ValidateGetQuery</c> method throws a <see cref="ArgumentNullException"/> when the
        /// specified <see cref="FluentQueryReader"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ValidateGetQueryWhenReaderNull()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var validator = new Mock<FluentQueryNameValidator>().Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.Object.InvokeValidateGetQuery(null, validator));
            Assert.That(exception.ParamName, Is.EqualTo("reader"));
        }

        /// <summary>
        /// Tests if the <c>ValidateGetQuery</c> method throws a <see cref="ArgumentNullException"/> when the
        /// specified <see cref="FluentQueryNameValidator"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ValidateGetQueryWhenQueryNull()
        {
            var query = new Mock<QueryPart>(null, null);
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var reader = new Mock<FluentQueryReader>(query.Object).Object;
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.Object.InvokeValidateGetQuery(reader, null));
            Assert.That(exception.ParamName, Is.EqualTo("validator"));
        }

        /// <summary>
        /// Tests if the <c>ValidateGetQuery</c> method works correctly when the specified arguments are valid.
        /// </summary>
        [Test]
        public void ValidateGetQueryWhenValidArgs()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var query = new Mock<QueryPart>(null, null);
            var reader = new Mock<FluentQueryReader>(query.Object).Object;
            var validator = new Mock<FluentQueryNameValidator>();
            validator.Protected().Setup<bool>("ValidateQueryPart", query.Object).Returns(true);

            repository.Object.InvokeValidateGetQuery(reader, validator.Object);

            validator.VerifyAll();
        }

        /// <summary>
        /// Tests the <c>Save</c> method when the specified fluent query is <c>null</c>.
        /// </summary>
        [Test]
        public void SaveWhenQueryNull()
        {
            FluentQueryRepositoryStub repository = (new Mock<FluentQueryRepositoryStub> { CallBase = true }).Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.Save(null));
            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests the <c>Save</c> method in the common case.
        /// </summary>
        [Test]
        public void SaveWhenCommonCase()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var query = new Mock<QueryPart>(null, null);

            Guid saveId = Guid.NewGuid();
            repository.Protected().Setup<Guid>("ExecuteSaveQuery", query.Object).Returns(saveId);

            Guid result = repository.Object.Save(query.Object);

            Assert.That(result, Is.EqualTo(saveId));
            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ExecuteSaveQuery</c> method throws a <see cref="ArgumentNullException"/> when the
        /// specified fluent query is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteSaveQueryWhenQueryNull()
        {
            FluentQueryRepositoryStub repository = (new Mock<FluentQueryRepositoryStub> { CallBase = true }).Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.InvokeExecuteSaveQuery(null));
            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests if the <c>ExecuteSaveQuery</c> method validates the input fluent query using the validator returned
        /// by the <c>CreateSaveQueryNameValidator</c> method.
        /// </summary>
        [Test]
        public void ExecuteSaveQueryCheckIfQueryIsValidated()
        {
            var query = new Mock<QueryPart>(null, "TestPart");

            var validator = new Mock<FluentQueryNameValidator> { CallBase = true };
            validator.Protected().Setup<bool>("ValidateQueryPart", query.Object).Returns(false);

            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            repository.Protected()
                .Setup<FluentQueryNameValidator>("CreateSaveQueryNameValidator")
                .Returns(validator.Object);

            var exception = Assert.Throws<QueryPartException>(
                () => repository.Object.InvokeExecuteSaveQuery(query.Object));

            Assert.That(
                exception.Message,
                Is.EqualTo("The fluent query contains an invalid or unsupported query part 'TestPart'."));

            validator.VerifyAll();
            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ExecuteSaveQuery</c> method works correctly when the fluent query contains a
        /// <see cref="IDataObject{TEntity,TId}"/> to save.
        /// </summary>
        [Test]
        public void ExecuteSaveQueryWhenDataObjectSpecified()
        {
            var data = new TestEntity();
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var query = new Mock<ValueQueryPart<TestEntity>>(null /*parent*/, FluentQueryCommonPartNames.Value, data);

            Guid expectedId = Guid.NewGuid();
            repository.Protected().Setup<Guid>("SaveDataObject", data).Returns(expectedId);
            
            Guid actualResult = repository.Object.InvokeExecuteSaveQuery(query.Object);
            
            Assert.That(actualResult, Is.EqualTo(expectedId));
            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ExecuteSaveQuery</c> method works correctly when the fluent query contains a custom save
        /// operation which is supported by the repository.
        /// </summary>
        [Test]
        public void ExecuteSaveQueryWhenSaveOperationSupported()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };

            Guid id = Guid.NewGuid();
            var query = new GuidQueryPart(null, FluentQueryCommonPartNames.Id, id);

            repository.Protected()
                .Setup<bool>("HandleSaveOperations", id, ItExpr.IsAny<FluentQueryReader>())
                .Returns(true /* operation handled */);
            
            Guid actualResult = repository.Object.InvokeExecuteSaveQuery(query);

            Assert.That(actualResult, Is.EqualTo(id));
            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ExecuteSaveQuery</c> method throws a <see cref="NotSupportedException"/> when the fluent
        /// query contains a custom save operation which is not supported by the repository.
        /// </summary>
        [Test]
        public void ExecuteSaveQueryWhenSaveOperationNotSupported()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };

            Guid id = Guid.NewGuid();
            var query = new GuidQueryPart(null, FluentQueryCommonPartNames.Id, id);

            repository.Protected()
                .Setup<bool>("HandleSaveOperations", id, ItExpr.IsAny<FluentQueryReader>())
                .Returns(false /* operation not handled */);

            NotSupportedException exception = Assert.Throws<NotSupportedException>(
                () => repository.Object.InvokeExecuteSaveQuery(query));
            
            Assert.That(exception.Message, Is.EqualTo("The specified query is not supported."));
            repository.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>ValidateSaveQuery</c> method throws a <see cref="ArgumentNullException"/> when the
        /// specified <see cref="FluentQueryReader"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ValidateSaveQueryWhenReaderNull()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var validator = new Mock<FluentQueryNameValidator>().Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.Object.InvokeValidateSaveQuery(null, validator));
            Assert.That(exception.ParamName, Is.EqualTo("reader"));
        }

        /// <summary>
        /// Tests if the <c>ValidateSaveQuery</c> method throws a <see cref="ArgumentNullException"/> when the
        /// specified <see cref="FluentQueryNameValidator"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ValidateSaveQueryWhenQueryNull()
        {
            var query = new Mock<QueryPart>(null, null);
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var reader = new Mock<FluentQueryReader>(query.Object).Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => repository.Object.InvokeValidateSaveQuery(reader, null));
            Assert.That(exception.ParamName, Is.EqualTo("validator"));
        }

        /// <summary>
        /// Tests if the <c>ValidateSaveQuery</c> method works correctly when the specified arguments are valid.
        /// </summary>
        [Test]
        public void ValidateSaveQueryWhenValidArgs()
        {
            var repository = new Mock<FluentQueryRepositoryStub> { CallBase = true };
            var query = new Mock<QueryPart>(null, null);
            var reader = new Mock<FluentQueryReader>(query.Object).Object;
            var validator = new Mock<FluentQueryNameValidator>();
            validator.Protected().Setup<bool>("ValidateQueryPart", query.Object).Returns(true);

            repository.Object.InvokeValidateSaveQuery(reader, validator.Object);

            validator.VerifyAll();
        }

        /// <summary>
        /// Test data object stub.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "Unit tests")]
        public class TestData : IDataObject<TestEntity, Guid>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestData"/> class.
            /// </summary>
            public TestData()
            {
                this.Id = Guid.NewGuid();
            }

            /// <summary>
            /// Gets or sets the identifier of the test object.
            /// </summary>
            public Guid Id { get; set; }

            /// <summary>
            /// Gets or sets the value of the test object.
            /// </summary>
            public string Value { get; set; }

            /// <summary>
            /// Creates a new logic-tier object which contains the data of this object.
            /// </summary>
            /// <returns>The created logic-tier object.</returns>
            public TestEntity ToEntity()
            {
                return new TestEntity { Value = this.Value };
            }
        }

        /// <summary>
        /// Test entity object stub.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "Unit tests")]
        public class TestEntity
        {
            /// <summary>
            /// Gets or sets the value of the test object.
            /// </summary>
            public string Value { get; set; }
        }

        /// <summary>
        /// Implements a subclass of <see cref="FluentQueryRepository{TData, TEntity, TId}"/> class for test purposes.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "Unit tests")]
        public abstract class FluentQueryRepositoryStub : FluentQueryRepository<TestData, TestEntity, Guid>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="FluentQueryRepositoryStub"/> class.
            /// </summary>
            protected FluentQueryRepositoryStub()
                : base(new Mock<SessionFactoryCache>().Object, new Mock<IDbConnection>().Object)
            {
            }

            /// <summary>
            /// Invokes the <c>ValidateGetQuery</c> method.
            /// </summary>
            /// <param name="reader">The fluent query reader instance.</param>
            /// <param name="validator">The fluent query validator which should be used to validate the query.</param>
            public void InvokeValidateGetQuery(FluentQueryReader reader, FluentQueryNameValidator validator)
            {
                this.ValidateGetQuery(reader, validator);
            }

            /// <summary>
            /// Executes the <c>ExecuteGetQuery</c> method.
            /// </summary>
            /// <param name="query">The fluent query to execute.</param>
            /// <returns>The result of the fluent query.</returns>
            public IEnumerable<TestData> InvokeExecuteGetQuery(QueryPart query)
            {
                return this.ExecuteGetQuery(query);
            }

            /// <summary>
            /// Invokes the <c>ValidateSaveQuery</c> method.
            /// </summary>
            /// <param name="reader">The fluent query reader instance.</param>
            /// <param name="validator">The fluent query validator which should be used to validate the query.</param>
            public void InvokeValidateSaveQuery(FluentQueryReader reader, FluentQueryNameValidator validator)
            {
                this.ValidateSaveQuery(reader, validator);
            }

            /// <summary>
            /// Executes the <c>ExecuteSaveQuery</c> method.
            /// </summary>
            /// <param name="query">The fluent query to execute.</param>
            /// <returns>The identifier of the saved entity object.</returns>
            public Guid InvokeExecuteSaveQuery(QueryPart query)
            {
                return this.ExecuteSaveQuery(query);
            }
        }
    }
}