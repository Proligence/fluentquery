﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Query{T}.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A surrogate object for serializing fluent query delegates.
    /// </summary>
    /// <typeparam name="T">Query type.</typeparam>
    [DataContract]
    public class Query<T> : Query where T : QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Query{T}"/> class.
        /// </summary>
        /// <param name="rootQueryPart">The root query part of the fluent query.</param>
        internal Query(QueryPart rootQueryPart)
            : base(rootQueryPart)
        {
        }

        /// <summary>
        /// During serialization, returns an object that substitutes the specified object.
        /// </summary>
        /// <param name="obj">The object to substitute.</param>
        /// <returns>The substituted object that will be serialized.</returns>
        public static implicit operator Query<T>(FluentQuery<T> obj)
        {
            if (obj != null)
            {
                var firstPart = Activator.CreateInstance<T>();
                var resultPart = obj(firstPart);
                return new Query<T>(resultPart);
            }

            return null;
        }

        /// <summary>
        /// During deserialization, returns an object that is a substitute for the specified object.
        /// </summary>
        /// <param name="obj">The deserialized object to be substituted.</param>
        /// <returns>The substituted deserialized object.</returns>
        public static implicit operator FluentQuery<T>(Query<T> obj)
        {
            if (obj != null)
            {
                return (FluentQuery<T>)(x => obj.RootQueryPart);
            }

            return null;
        }
    }
}
