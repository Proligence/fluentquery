﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TupleQueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Implements a query part which represents a single named value.
    /// </summary>
    /// <typeparam name="T1">The type of the first value stored in the tuple.</typeparam>
    /// <typeparam name="T2">The type of the second value stored in the tuple.</typeparam>
    [DataContract]
    public abstract class TupleQueryPart<T1, T2> : QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TupleQueryPart{T1,T2}"/> class.
        /// </summary>
        /// <param name="parentPart">The parent query part.</param>
        /// <param name="name">The name of the query part.</param>
        /// <param name="value1">The first value.</param>
        /// <param name="value2">The second value.</param>
        protected TupleQueryPart(QueryPart parentPart, string name, T1 value1, T2 value2)
            : base(parentPart, name)
        {
            this.Value1 = value1;
            this.Value2 = value2;
        }

        /// <summary>
        /// Gets the first value.
        /// </summary>
        [DataMember]
        public T1 Value1 { get; private set; }

        /// <summary>
        /// Gets the second value.
        /// </summary>
        [DataMember]
        public T2 Value2 { get; private set; }

        /// <summary>
        /// Returns a <see cref="string"/> which represents the contents of the query part.
        /// </summary>
        /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
        public override string GetContentString()
        {
            string value1 = !ReferenceEquals(this.Value1, null) ? this.Value1.ToString() : string.Empty;
            string value2 = !ReferenceEquals(this.Value2, null) ? this.Value2.ToString() : string.Empty;
            
            return value1 + "," + value2;
        }

        /// <summary>
        /// Compares the content of the current query part to the content of the specified <see cref="QueryPart"/>.
        /// </summary>
        /// <param name="otherPart">The other query part to compare.</param>
        /// <returns>
        /// <c>true</c> if the content of the specified query part is equal to the content of the current query part;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CompareContent(QueryPart otherPart)
        {
            TupleQueryPart<T1, T2> tuplePart = otherPart as TupleQueryPart<T1, T2>;
            if (tuplePart != null)
            {
                if (!QueryPart.ValuesEqual(this.Value1, tuplePart.Value1))
                {
                    return false;
                }

                if (!QueryPart.ValuesEqual(this.Value2, tuplePart.Value2))
                {
                    return false;
                }

                return true;
            }

            return false;
        }
    }
}