﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GuidListQueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;

    /// <summary>
    /// Implements a query part which stores a list of <see cref="Guid"/> values.
    /// </summary>
    [DataContract]
    public class GuidListQueryPart : ListQueryPart<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GuidListQueryPart"/> class.
        /// </summary>
        /// <param name="parentPart">The parent query part.</param>
        /// <param name="name">The name of the query part.</param>
        /// <param name="values">The values which will be stored in the query part.</param>
        [ExcludeFromCodeCoverage]
        public GuidListQueryPart(QueryPart parentPart, string name, IEnumerable<Guid> values)
            : base(parentPart, name, values)
        {
        }
    }
}