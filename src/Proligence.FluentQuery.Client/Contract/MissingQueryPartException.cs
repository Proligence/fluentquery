﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MissingQueryPartException.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines the exception which is thrown when an expected query part is missing in a fluent query.
    /// </summary>
    [Serializable]
    public class MissingQueryPartException : QueryPartException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MissingQueryPartException"/> class.
        /// </summary>
        public MissingQueryPartException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingQueryPartException"/> class.
        /// </summary>
        /// <param name="queryPartType">The type of the missing query part.</param>
        /// <param name="queryPartName">The name of the missing query part.</param>
        public MissingQueryPartException(Type queryPartType, string queryPartName)
            : base(GetMessage(queryPartType, queryPartName), queryPartType, queryPartName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingQueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        public MissingQueryPartException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingQueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        /// <param name="queryPartType">The type of the missing query part.</param>
        /// <param name="queryPartName">The name of the missing query part.</param>
        public MissingQueryPartException(string message, Type queryPartType, string queryPartName)
            : base(message, queryPartType, queryPartName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingQueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        /// <param name="inner">The exception which caused this exception.</param>
        public MissingQueryPartException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingQueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        /// <param name="inner">The exception which caused this exception.</param>
        /// <param name="queryPartType">The type of the missing query part.</param>
        /// <param name="queryPartName">The name of the missing query part.</param>
        public MissingQueryPartException(string message, Exception inner, Type queryPartType, string queryPartName)
            : base(message, inner, queryPartType, queryPartName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MissingQueryPartException"/> class.
        /// </summary>
        /// <param name="info">
        /// The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being
        /// thrown.
        /// </param>
        /// <param name="context">
        /// The <see cref="StreamingContext"/> that contains contextual information about the source or destination.
        /// </param>
        protected MissingQueryPartException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Gets the default exception message for the specified query part type and/or name.
        /// </summary>
        /// <param name="queryPartType">The type of the missing query part.</param>
        /// <param name="queryPartName">The name of the missing query part.</param>
        /// <returns>The exception message.</returns>
        private static string GetMessage(Type queryPartType, string queryPartName)
        {
            string message = "Failed to find query part";

            if (!string.IsNullOrEmpty(queryPartName))
            {
                message += " with name '" + queryPartName + "'";
            }

            if (queryPartType != null)
            {
                message += " of type '" + queryPartType.Name + "'";
            }

            return message + ".";
        }
    }
}