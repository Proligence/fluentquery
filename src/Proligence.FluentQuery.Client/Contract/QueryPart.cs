﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a part of a fluent query.
    /// </summary>
    [DataContract]
    public abstract class QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPart"/> class.
        /// </summary>
        /// <param name="parentPart">The parent query part.</param>
        /// <param name="name">The name of the query part.</param>
        protected QueryPart(QueryPart parentPart = null, string name = null)
        {
            this.ParentPart = parentPart;
            this.Name = name;
        }

        /// <summary>
        /// Gets the name of the query part.
        /// </summary>
        [DataMember]
        internal string Name { get; private set; }

        /// <summary>
        /// Gets the parent query part.
        /// </summary>
        [DataMember]
        internal QueryPart ParentPart { get; private set; }

        /// <summary>
        /// Returns a <see cref="string"/> which represents the contents of the query part.
        /// </summary>
        /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "By design.")]
        public abstract string GetContentString();

        /// <summary>
        /// Compares the content of the current query part to the content of the specified <see cref="QueryPart"/>.
        /// </summary>
        /// <param name="otherPart">The other query part to compare.</param>
        /// <returns>
        /// <c>true</c> if the content of the specified query part is equal to the content of the current query part;
        /// otherwise, <c>false</c>.
        /// </returns>
        public virtual bool CompareContent(QueryPart otherPart)
        {
            if (otherPart == null)
            {
                throw new ArgumentNullException("otherPart");
            }

            return this.GetContentString().Equals(otherPart.GetContentString());
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="string"/> that represents this instance.</returns>
        public override string ToString()
        {
            string result = "[";

            string contentString = this.GetContentString();

            if (!string.IsNullOrEmpty(this.Name))
            {
                result += this.Name;

                if (!string.IsNullOrEmpty(contentString))
                {
                    result += "|";
                }
            }

            if (!string.IsNullOrEmpty(contentString))
            {
                result += contentString;
            }

            result += "]";

            if (this.ParentPart != null)
            {
                result += "->" + this.ParentPart;
            }

            return result;
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj, false);
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with this instance.</param>
        /// <param name="recursive"><c>true</c> to also compare parent nodes; otherwise, <c>false</c>.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public bool Equals(object obj, bool recursive)
        {
            if ((obj == null) || (obj.GetType() != this.GetType()))
            {
                return false;
            }

            QueryPart queryPart = (QueryPart)obj;
            if (this.Name != queryPart.Name)
            {
                return false;
            }

            if (recursive)
            {
                if ((this.ParentPart != null) && !this.ParentPart.Equals(queryPart.ParentPart))
                {
                    return false;
                }
            }

            return this.CompareContent(queryPart);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash
        /// table.
        /// </returns>
        public override int GetHashCode()
        {
            return (this.Name != null ? this.Name.GetHashCode() : 0) ^ this.GetContentString().GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified values are equal.
        /// </summary>
        /// <typeparam name="T">The type of values to compare.</typeparam>
        /// <param name="value1">The first value to compare.</param>
        /// <param name="value2">The other value to compare.</param>
        /// <returns><c>true</c> if the specified values are equal; otherwise, <c>false</c>.</returns>
        protected static bool ValuesEqual<T>(T value1, T value2)
        {
            if (ReferenceEquals(value1, value2))
            {
                return true;
            }

            bool value1Null = ReferenceEquals(value1, null);
            bool value2Null = ReferenceEquals(value2, null);

            if (value1Null || value2Null)
            {
                return value1Null && value2Null;
            }

            return value1.Equals(value2);
        }
    }
}