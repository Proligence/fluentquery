﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NoValueQueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The base class for fluent query classes which do not contain any value.
    /// </summary>
    [DataContract]
    public abstract class NoValueQueryPart : QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoValueQueryPart"/> class.
        /// </summary>
        protected NoValueQueryPart()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoValueQueryPart"/> class.
        /// </summary>
        /// <param name="parentPart">The parent query part.</param>
        protected NoValueQueryPart(QueryPart parentPart)
            : base(parentPart)
        {
        }

        /// <summary>
        /// Returns a <see cref="string"/> which represents the contents of the query part.
        /// </summary>
        /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
        public override string GetContentString()
        {
            return this.GetType().Name;
        }
    }
}