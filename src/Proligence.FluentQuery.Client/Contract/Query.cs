// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Query.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The base class for classes which represent fluent queries.
    /// </summary>
    [DataContract]
    public class Query
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Query"/> class.
        /// </summary>
        /// <param name="rootQueryPart">The root query part of the fluent query.</param>
        internal Query(QueryPart rootQueryPart)
        {
            if (rootQueryPart == null)
            {
                throw new ArgumentNullException("rootQueryPart");
            }

            this.RootQueryPart = rootQueryPart;
        }

        /// <summary>
        /// Gets the root query part of the fluent query.
        /// </summary>
        [DataMember]
        public QueryPart RootQueryPart { get; private set; }
    }
}