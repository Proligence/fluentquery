﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryCommonPartNames.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Defines common, well-known names for fluent query <see cref="QueryPart"/> objects.
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors",
        Justification = "By design - we need to inherit from this class to extend common query part names.")]
    public class FluentQueryCommonPartNames
    {
        /// <summary>
        /// Specifies the value of a globally unique object identifier (GUID).
        /// </summary>
        public const string Id = "Id";

        /// <summary>
        /// Specifies a sequence of globally unique object identifiers (GUIDs).
        /// </summary>
        public const string Ids = "Ids";

        /// <summary>
        /// Specifies the main value for an operation.
        /// </summary>
        public const string Value = "Value";

        /// <summary>
        /// The name of the switch which specifies that all objects of the specified type should be returned.
        /// </summary>
        public const string All = "All";

        /// <summary>
        /// Specifies the number of items for a fluent query operation.
        /// </summary>
        public const string Count = "Count";

        /// <summary>
        /// The name of the switch which specifies that an object should be saved with all subobjects.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Subobjects",
            Justification = "By design.")]
        public const string SaveIncludeSubobjects = "SaveIncludeSubobjects";

        /// <summary>
        /// Initializes a new instance of the <see cref="FluentQueryCommonPartNames"/> class.
        /// </summary>
        /// <remarks>
        /// This sole purpose of this constructor is to allow inheriting from this subclass to add common
        /// project-specific well-known names. However, this constructor should never be called.
        /// </remarks>
        protected FluentQueryCommonPartNames()
        {
        }
    }
}