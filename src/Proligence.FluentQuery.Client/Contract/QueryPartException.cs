﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueryPartException.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The exception that is thrown when a missing, invalid or unsupported query part is used.
    /// </summary>
    [Serializable]
    public class QueryPartException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPartException"/> class.
        /// </summary>
        public QueryPartException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        public QueryPartException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        /// <param name="queryPartType">The type of the query part associated with the exception.</param>
        /// <param name="queryPartName">The name of the query part associated with the exception.</param>
        public QueryPartException(string message, Type queryPartType, string queryPartName)
            : base(message)
        {
            this.QueryPartType = queryPartType;
            this.QueryPartName = queryPartName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        /// <param name="inner">The exception which caused this exception.</param>
        public QueryPartException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPartException"/> class.
        /// </summary>
        /// <param name="message">The message which describes the error which caused the exception.</param>
        /// <param name="inner">The exception which caused this exception.</param>
        /// <param name="queryPartType">The type of the query part associated with the exception.</param>
        /// <param name="queryPartName">The name of the query part associated with the exception.</param>
        public QueryPartException(string message, Exception inner, Type queryPartType, string queryPartName)
            : base(message, inner)
        {
            this.QueryPartType = queryPartType;
            this.QueryPartName = queryPartName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPartException"/> class.
        /// </summary>
        /// <param name="info">
        /// The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being
        /// thrown.
        /// </param>
        /// <param name="context">
        /// The <see cref="StreamingContext"/> that contains contextual information about the source or destination.
        /// </param>
        protected QueryPartException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.QueryPartType = (Type)info.GetValue("QueryPartType", typeof(Type));
            this.QueryPartName = info.GetString("QueryPartName");
        }

        /// <summary>
        /// Gets the type of the missing query part.
        /// </summary>
        public Type QueryPartType { get; private set; }

        /// <summary>
        /// Gets the name of the missing query part.
        /// </summary>
        public string QueryPartName { get; private set; }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="SerializationInfo"/> with information about the
        /// exception.
        /// </summary>
        /// <param name="info">
        /// The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being
        /// thrown.
        /// </param>
        /// <param name="context">
        /// The <see cref="StreamingContext"/> that contains contextual information about the source or destination.
        /// </param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("QueryPartType", this.QueryPartType);
            info.AddValue("QueryPartName", this.QueryPartName);
        }
    }
}