﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FinalQueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a query part which does not provide any more extensions.
    /// </summary>
    [DataContract]
    public class FinalQueryPart : QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FinalQueryPart"/> class.
        /// </summary>
        /// <param name="parentPart">The parent part.</param>
        public FinalQueryPart(QueryPart parentPart = null)
            : base(parentPart)
        {
        }

        /// <summary>
        /// Returns a <see cref="string"/> which represents the contents of the query part.
        /// </summary>
        /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
        public override string GetContentString()
        {
            return string.Empty;
        }

        /// <summary>
        /// Compares the content of the current query part to the content of the specified <see cref="QueryPart"/>.
        /// </summary>
        /// <param name="otherPart">The other query part to compare.</param>
        /// <returns>
        /// <c>true</c> if the content of the specified query part is equal to the content of the current query part;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CompareContent(QueryPart otherPart)
        {
            return otherPart is FinalQueryPart;
        }
    }
}