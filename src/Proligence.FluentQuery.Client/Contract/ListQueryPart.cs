﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListQueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Implements a query part which represents a list of named values.
    /// </summary>
    /// <typeparam name="T">The type of values stored in the list.</typeparam>
    [DataContract]
    public abstract class ListQueryPart<T> : QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListQueryPart{T}"/> class.
        /// </summary>
        /// <param name="parentPart">The parent query part.</param>
        /// <param name="name">The name of the list.</param>
        /// <param name="values">The list's values.</param>
        protected ListQueryPart(QueryPart parentPart, string name, IEnumerable<T> values)
            : base(parentPart, name)
        {
            this.Values = values;
        }

        /// <summary>
        /// Gets the values stored in the list.
        /// </summary>
        [DataMember]
        public IEnumerable<T> Values { get; private set; }

        /// <summary>
        /// Returns a <see cref="string"/> which represents the contents of the query part.
        /// </summary>
        /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
        public override string GetContentString()
        {
            if (object.ReferenceEquals(this.Values, null))
            {
                return "null";
            }

            return string.Join(",", this.Values);
        }

        /// <summary>
        /// Compares the content of the current query part to the content of the specified <see cref="QueryPart"/>.
        /// </summary>
        /// <param name="otherPart">The other query part to compare.</param>
        /// <returns>
        /// <c>true</c> if the content of the specified query part is equal to the content of the current query part;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CompareContent(QueryPart otherPart)
        {
            ListQueryPart<T> listPart = otherPart as ListQueryPart<T>;
            if (listPart == null)
            {
                return false;
            }

            if (object.ReferenceEquals(this.Values, null) || object.ReferenceEquals(listPart.Values, null))
            {
                return object.ReferenceEquals(this.Values, null) && object.ReferenceEquals(listPart.Values, null);
            }

            if (this.Values.Count() != listPart.Values.Count())
            {
                return false;
            }

            IEnumerator<T> enum1 = this.Values.GetEnumerator();
            IEnumerator<T> enum2 = listPart.Values.GetEnumerator();

            while (enum1.MoveNext() && enum2.MoveNext())
            {
                if (!QueryPart.ValuesEqual(enum1.Current, enum2.Current))
                {
                    return false;
                }
            }

            return true;
        }
    }
}