﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SwitchQueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The base class for query parts which represent boolean (on/off) switches.
    /// </summary>
    [DataContract]
    public class SwitchQueryPart : QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchQueryPart"/> class.
        /// </summary>
        /// <param name="parentPart">The parent query part.</param>
        /// <param name="name">The name of the query part.</param>
        /// <param name="enabled">
        /// A value indicating whether the switch represented by this <see cref="SwitchQueryPart"/> is enabled.
        /// </param>
        public SwitchQueryPart(QueryPart parentPart, string name, bool enabled)
            : base(parentPart, name)
        {
            this.Enabled = enabled;
        }

        /// <summary>
        /// Gets a value indicating whether the switch represented by this <see cref="SwitchQueryPart"/> is enabled.
        /// </summary>
        [DataMember]
        public bool Enabled { get; private set; }

        /// <summary>
        /// Returns a <see cref="string"/> which represents the contents of the query part.
        /// </summary>
        /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
        public override string GetContentString()
        {
            return this.Enabled.ToString();
        }

        /// <summary>
        /// Compares the content of the current query part to the content of the specified <see cref="QueryPart"/>.
        /// </summary>
        /// <param name="otherPart">The other query part to compare.</param>
        /// <returns>
        /// <c>true</c> if the content of the specified query part is equal to the content of the current query part;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CompareContent(QueryPart otherPart)
        {
            SwitchQueryPart switchPart = otherPart as SwitchQueryPart;
            if (switchPart != null)
            {
                return this.Enabled == switchPart.Enabled;
            }

            return false;
        }
    }
}