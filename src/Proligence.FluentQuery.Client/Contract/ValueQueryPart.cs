﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValueQueryPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Contract
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Implements a query part which represents a single named value.
    /// </summary>
    /// <typeparam name="T">The type of stored value.</typeparam>
    [DataContract]
    public abstract class ValueQueryPart<T> : QueryPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValueQueryPart{T}"/> class.
        /// </summary>
        /// <param name="parentPart">The parent query part.</param>
        /// <param name="name">The name of the query part.</param>
        /// <param name="value">The value to store in the query part.</param>
        protected ValueQueryPart(QueryPart parentPart, string name, T value)
            : base(parentPart, name)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the value stored in the query part.
        /// </summary>
        [DataMember]
        public T Value { get; private set; }

        /// <summary>
        /// Returns a <see cref="string"/> which represents the contents of the query part.
        /// </summary>
        /// <returns>A <see cref="string"/> which represents the contents of the query part.</returns>
        public override string GetContentString()
        {
            if (object.ReferenceEquals(this.Value, null))
            {
                return "null";
            }

            return this.Value.ToString();
        }

        /// <summary>
        /// Compares the content of the current query part to the content of the specified <see cref="QueryPart"/>.
        /// </summary>
        /// <param name="otherPart">The other query part to compare.</param>
        /// <returns>
        /// <c>true</c> if the content of the specified query part is equal to the content of the current query part;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CompareContent(QueryPart otherPart)
        {
            ValueQueryPart<T> valuePart = otherPart as ValueQueryPart<T>;
            if (valuePart != null)
            {
                if (object.ReferenceEquals(this.Value, valuePart.Value))
                {
                    return true;
                }
                
                if (!object.ReferenceEquals(this.Value, null) && !object.ReferenceEquals(valuePart.Value, null))
                {
                    return this.Value.Equals(valuePart.Value);
                }
            }

            return base.CompareContent(otherPart);
        }
    }
}