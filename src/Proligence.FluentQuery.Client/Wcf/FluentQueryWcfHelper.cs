﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryWcfHelper.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Wcf
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Description;

    using Proligence.Astoria.Client.Serialization;
    using Proligence.FluentQuery.Client.Contract;

    using ProtoBuf.Meta;
    using ProtoBuf.ServiceModel;

    /// <summary>
    /// Provides helper methods for integrating FluentQuery with WCF services.
    /// </summary>
    public static class FluentQueryWcfHelper
    {
        /// <summary>
        /// The sync root.
        /// </summary>
        private static readonly object SyncRoot = new object();

        /// <summary>
        /// The cached types.
        /// </summary>
        private static volatile IEnumerable<Type> cachedTypes;

        /// <summary>
        /// Sets up FluentQuery for the specified WCF <see cref="ServiceHost"/>.
        /// </summary>
        /// <param name="serviceHost">The WCF <see cref="ServiceHost"/> of the service to set up.</param>
        public static void SetupFluentQueryForServiceHost(this ServiceHostBase serviceHost)
        {
            if (serviceHost == null)
            {
                throw new ArgumentNullException("serviceHost");
            }

            foreach (ServiceEndpoint endpoint in serviceHost.Description.Endpoints)
            {
                SetupFluentQueryForEndpoint(endpoint);
            }
        }

        /// <summary>
        /// Sets up FluentQuery for the specified WCF <see cref="ServiceEndpoint"/>.
        /// </summary>
        /// <param name="endpoint">The WCF <see cref="ServiceEndpoint"/> to set up.</param>
        public static void SetupFluentQueryForEndpoint(this ServiceEndpoint endpoint)
        {
            if (endpoint == null)
            {
                throw new ArgumentNullException("endpoint");
            }

            if (!endpoint.Contract.ContractType.IsDefined(typeof(UsesFluentQueryAttribute), true))
            {
                return;
            }

            if (cachedTypes == null)
            {
                lock (SyncRoot)
                {
                    if (cachedTypes == null)
                    {
                        var queryTypes = FluentQueryKnownTypes.GetQueryPartTypes().ConstructIdentifiers();
                        var queryClosedTypes = new HashSet<Type>();

                        foreach (var kvp in queryTypes)
                        {
                            var queryType = typeof(FluentQuery<>).MakeGenericType(kvp.Value);
                            var surrogateType = typeof(Query<>).MakeGenericType(kvp.Value);

                            if (!RuntimeTypeModel.Default.ContainsType(surrogateType))
                            {
                                RuntimeTypeModel.Default.RegisterType(surrogateType);
                                RuntimeTypeModel.Default[typeof(Query)].SafelyAddSubType(kvp.Key, surrogateType);
                            }

                            if (!RuntimeTypeModel.Default.ContainsType(queryType))
                            {
                                RuntimeTypeModel.Default.RegisterType(queryType);
                            }
                                 
                            RuntimeTypeModel.Default[queryType].SetSurrogate(surrogateType);

                            queryClosedTypes.Add(queryType);
                            queryClosedTypes.Add(surrogateType);
                        }

                        cachedTypes = queryClosedTypes;
                    }
                }
            }

            foreach (OperationDescription op in endpoint.Contract.Operations)
            {
                var protoBehavior = op.Behaviors.Find<ProtoOperationBehavior>();
                if (protoBehavior == null)
                {
                    protoBehavior = new ProtoOperationBehavior(op);
                    op.Behaviors.Add(protoBehavior);
                }

                foreach (var type in cachedTypes)
                {
                    op.KnownTypes.Add(type);
                }
            }
        }
    }
}