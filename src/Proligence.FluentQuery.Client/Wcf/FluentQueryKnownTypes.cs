﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryKnownTypes.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Client.Wcf
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using Proligence.FluentQuery.Client.Contract;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements the WCF known type provider for serializing fluent queries.
    /// </summary>
    public static class FluentQueryKnownTypes
    {
        /// <summary>
        /// Gets known types.
        /// </summary>
        /// <param name="provider">The <see cref="ICustomAttributeProvider"/> instance.</param>
        /// <returns>Sequence of types.</returns>
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "provider",
            Justification = "Parameter required by WCF API.")]
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            var types = new List<Type>();
            types.Add(Type.GetType("System.DelegateSerializationHolder+DelegateEntry"));
            types.AddRange(GetQueryPartTypes());

            return types;
        }

        /// <summary>
        /// Gets all types which inherit from <see cref="QueryPart"/> class.
        /// </summary>
        /// <returns>Sequence of types.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "By design.")]
        public static IEnumerable<Type> GetQueryPartTypes()
        {
            return AppDomain
                .CurrentDomain
                .GetAssemblies()
                .SelectMany(
                    a => a.GetLoadableTypes().Where(t => typeof(QueryPart).IsAssignableFrom(t) && !t.IsGenericType))
                .ToArray();
        }
    }
}