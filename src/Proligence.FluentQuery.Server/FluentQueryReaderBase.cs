﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryReaderBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// The base class for classes which implement reading fluent queries.
    /// </summary>
    public abstract class FluentQueryReaderBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FluentQueryReaderBase"/> class.
        /// </summary>
        /// <param name="queryRootPart">The root part of the fluent query.</param>
        protected FluentQueryReaderBase(QueryPart queryRootPart)
        {
            if (queryRootPart == null)
            {
                throw new ArgumentNullException("queryRootPart");
            }

            this.QueryRootPart = queryRootPart;
        }

        /// <summary>
        /// Gets the root part of the fluent query.
        /// </summary>
        public QueryPart QueryRootPart { get; private set; }

        /// <summary>
        /// Gets the specified parts of the fluent query.
        /// </summary>
        /// <typeparam name="TPart">The type of the query parts to get.</typeparam>
        /// <param name="name">The name of the query parts to get or <c>null</c>.</param>
        /// <returns>A sequence which contains the specified query parts.</returns>
        public abstract IEnumerable<TPart> GetQueryParts<TPart>(string name = null)
            where TPart : QueryPart;

        /// <summary>
        /// Gets the specified part of the fluent query.
        /// </summary>
        /// <typeparam name="TPart">The type of the query part to get.</typeparam>
        /// <param name="name">The name of the query part to get or <c>null</c>.</param>
        /// <returns>The specified query part or <c>null</c> if the specified query part was not found.</returns>
        public virtual TPart GetQueryPart<TPart>(string name = null)
            where TPart : QueryPart
        {
            return this.GetQueryParts<TPart>(name).FirstOrDefault();
        }

        /// <summary>
        /// Gets the value of the specified <see cref="SwitchQueryPart"/> of the fluent query.
        /// </summary>
        /// <typeparam name="TPart">The type of the query part.</typeparam>
        /// <param name="name">The name of the <see cref="SwitchQueryPart"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="SwitchQueryPart.Enabled"/> value of the specified <see cref="SwitchQueryPart"/> or
        /// <c>false</c> if the specified query part was not found.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "By design.")]
        public virtual bool GetSwitch<TPart>(string name = null)
            where TPart : SwitchQueryPart
        {
            TPart queryPart = this.GetQueryPart<TPart>(name);
            if (queryPart != null)
            {
                return queryPart.Enabled;
            }

            return false;
        }

        /// <summary>
        /// Gets the value of the specified <see cref="ValueQueryPart{T}"/> of the fluent query.
        /// </summary>
        /// <typeparam name="TPart">The type of the query part.</typeparam>
        /// <typeparam name="TValue">
        /// The type of the value stored in the specified <see cref="ValueQueryPart{T}"/>.
        /// </typeparam>
        /// <param name="name">The name of the <see cref="ValueQueryPart{T}"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="ValueQueryPart{T}.Value"/> value of the specified <see cref="ValueQueryPart{T}"/> or
        /// <c>default(<typeparamref name="TValue"/>)</c> if the specified query part was not found.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "By design.")]
        public virtual TValue GetValue<TPart, TValue>(string name = null)
            where TPart : ValueQueryPart<TValue>
        {
            TPart queryPart = this.GetQueryPart<TPart>(name);
            if (queryPart != null)
            {
                return queryPart.Value;
            }

            return default(TValue);
        }

        /// <summary>
        /// Gets the value of the specified <see cref="ListQueryPart{T}"/> of the fluent query.
        /// </summary>
        /// <typeparam name="TPart">The type of the query part.</typeparam>
        /// <typeparam name="TItem">
        /// The type of items stored in the specified <see cref="ListQueryPart{T}"/>.
        /// </typeparam>
        /// <param name="name">The name of the <see cref="ListQueryPart{T}"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="ListQueryPart{T}.Values"/> value of the specified <see cref="ListQueryPart{T}"/> or
        /// <c>null</c> if the specified query part was not found.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "By design.")]
        public virtual IEnumerable<TItem> GetList<TPart, TItem>(string name = null)
            where TPart : ListQueryPart<TItem>
        {
            TPart queryPart = this.GetQueryPart<TPart>(name);
            if (queryPart != null)
            {
                return queryPart.Values;
            }

            return null;
        }
    }
}