﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server.NHibernate
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using global::NHibernate;
    using global::NHibernate.Criterion;
    using Proligence.FluentQuery.Client.Contract;
    using Proligence.Helpers;
    using Proligence.Helpers.Data;
    using Proligence.Helpers.NHibernate;

    /// <summary>
    /// The base class for fluent query-based NHibernate data repositories.
    /// </summary>
    /// <typeparam name="TData">
    /// The type of the wrapper which contains data returned from NHibernate.
    /// </typeparam>
    /// <typeparam name="TEntity">
    /// The type which represents the object's data in the application's logic tier.
    /// </typeparam>
    /// <typeparam name="TId">
    /// The type which represents the unique identifier of the entity objects.
    /// </typeparam>
    public abstract class FluentQueryRepository<TData, TEntity, TId> 
        : RepositoryBase, IFluentQueryRepository<TEntity, TId>
        where TData : IDataObject<TEntity, TId>
        where TEntity : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FluentQueryRepository{TData, TEntity, TId}"/> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        protected FluentQueryRepository(SessionFactoryCache factoryCache, IDbConnection connection)
            : base(factoryCache, connection)
        {
        }

        /// <summary>
        /// Gets the user profiles returned by the specified fluent query.
        /// </summary>
        /// <param name="query">The fluent query which specifies the user profiles to get.</param>
        /// <returns>A sequence of user profiles returned by the fluent query.</returns>
        public IEnumerable<TEntity> GetObjects(QueryPart query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return this.ExecuteGetQuery(query).Select(x => x.ToEntity());
        }

        /// <summary>
        /// Gets the identifiers of the user profiles returned by the specified fluent query.
        /// </summary>
        /// <param name="query">The fluent query which specifies the user profiles to get.</param>
        /// <returns>A sequence of identifiers of the user profiles returned by the fluent query.</returns>
        public IEnumerable<TId> GetIds(QueryPart query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return this.ExecuteGetQuery(query).Select(x => x.Id);
        }

        /// <summary>
        /// Creates or updates the specified user profile.
        /// </summary>
        /// <param name="query">The fluent query which specifies the user profile to save.</param>
        /// <returns>The identifier of the saved user profile.</returns>
        public TId Save(QueryPart query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return this.ExecuteSaveQuery(query);
        }

        /// <summary>
        /// Executes a fluent query for data selection.
        /// </summary>
        /// <param name="query">The fluent query to execute.</param>
        /// <returns>The result of the fluent query.</returns>
        protected virtual IEnumerable<TData> ExecuteGetQuery(QueryPart query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            var reader = new FluentQueryReader(query);
            this.ValidateGetQuery(reader, this.CreateGetQueryNameValidator());

            DetachedCriteria criteria = this.BuildGetCriteria(reader);

            return this.ExecuteGetCriteria(criteria, reader);
        }

        /// <summary>
        /// Creates a <see cref="FluentQueryNameValidator"/> for validating the fluent query for data selection.
        /// </summary>
        /// <returns>The created fluent query validator.</returns>
        protected virtual FluentQueryNameValidator CreateGetQueryNameValidator()
        {
            return new FluentQueryNameValidator();
        }

        /// <summary>
        /// Validates the query for data selection and throws an exception if the query is invalid.
        /// </summary>
        /// <param name="reader">The fluent query reader instance.</param>
        /// <param name="validator">The fluent query validator which should be used to validate the query.</param>
        protected virtual void ValidateGetQuery(FluentQueryReader reader, FluentQueryNameValidator validator)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (validator == null)
            {
                throw new ArgumentNullException("validator");
            }

            validator.Validate(reader.QueryRootPart);
        }

        /// <summary>
        /// Builds a NHibernate <see cref="DetachedCriteria"/> from a fluent query for data selection.
        /// </summary>
        /// <param name="reader">The fluent query reader instance.</param>
        /// <returns>The created detached query.</returns>
        protected abstract DetachedCriteria BuildGetCriteria(FluentQueryReaderBase reader);

        /// <summary>
        /// Executes an NHibernate <see cref="DetachedCriteria"/>.
        /// </summary>
        /// <param name="criteria">The criteria to execute.</param>
        /// <param name="reader">The reader of the fluent query from which the criteria was built.</param>
        /// <returns>The results returned by the executed criteria.</returns>
        [ExcludeFromCodeCoverage]
        protected virtual IEnumerable<TData> ExecuteGetCriteria(
            DetachedCriteria criteria,
            FluentQueryReaderBase reader)
        {
            return this.UsingSession(
                (session, transaction) =>
                {
                    IList<TData> results = criteria.GetExecutableCriteria(session).List<TData>();
                    this.PrepareGetResults(results, reader, session);

                    return results;
                });
        }

        /// <summary>
        /// Prepares results of a fluent query for data selection.
        /// </summary>
        /// <param name="results">The results returned by the fluent query.</param>
        /// <param name="reader">The fluent query reader instance.</param>
        /// <param name="session">The NHibernate session.</param>
        protected virtual void PrepareGetResults(IList<TData> results, FluentQueryReaderBase reader, ISession session)
        {
        }

        /// <summary>
        /// Executes a fluent query for saving data.
        /// </summary>
        /// <param name="query">The fluent query to execute.</param>
        /// <returns>The identifier of the saved entity object.</returns>
        protected virtual TId ExecuteSaveQuery(QueryPart query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            var reader = new FluentQueryReader(query);
            this.ValidateSaveQuery(reader, this.CreateSaveQueryNameValidator());

            TEntity entityObject = reader.GetValue<TEntity>(FluentQueryCommonPartNames.Value);
            if (entityObject != null)
            {
                return this.SaveDataObject(entityObject);
            }

            TId entityId = reader.GetRequiredValue<TId>(FluentQueryCommonPartNames.Id);

            if (this.HandleSaveOperations(entityId, reader))
            {
                return entityId;
            }

            throw new NotSupportedException("The specified query is not supported.");
        }

        /// <summary>
        /// Creates a <see cref="FluentQueryNameValidator"/> for validating the fluent query for saving data.
        /// </summary>
        /// <returns>The created fluent query validator.</returns>
        protected virtual FluentQueryNameValidator CreateSaveQueryNameValidator()
        {
            var validator = new FluentQueryNameValidator();
            validator.AddQueryPartNames(new[] { FluentQueryCommonPartNames.Value, FluentQueryCommonPartNames.Id });

            return validator;
        }

        /// <summary>
        /// Validates the query for saving data and throws an exception if the query is invalid.
        /// </summary>
        /// <param name="reader">The fluent query reader instance.</param>
        /// <param name="validator">The fluent query validator which should be used to validate the query.</param>
        protected virtual void ValidateSaveQuery(FluentQueryReader reader, FluentQueryNameValidator validator)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (validator == null)
            {
                throw new ArgumentNullException("validator");
            }

            validator.Validate(reader.QueryRootPart);
        }

        /// <summary>
        /// Saves the specified entity object to the database.
        /// </summary>
        /// <param name="entityObject">The entity object to save.</param>
        /// <returns>The identifier of the created or updated object.</returns>
        [ExcludeFromCodeCoverage]
        protected virtual TId SaveDataObject(TEntity entityObject)
        {
            return this.UsingSession(
                (session, transaction) =>
                {
                    TData dataObject = this.GetDataObjectEntity(entityObject, session);

                    var versionedEntity = entityObject as IVersioned<TId>;
                    var versionedDataObject = dataObject as IVersionedDataObject<TEntity, TId>;
                    if ((versionedEntity != null) && (versionedDataObject != null))
                    {
                        if (!versionedDataObject.VersionId.Equals(versionedEntity.VersionId))
                        {
                            throw new DBConcurrencyException(
                                "The update was performed on a version of the data which is no longer up-to-date.");
                        }
                    }

                    this.PrepareEntityForSave(entityObject, session);
                    
                    session.SaveOrUpdate(dataObject);
                    session.Flush();

                    this.InitializeSavedEntity(entityObject, dataObject, session);

                    return dataObject.Id;
                });
        }

        /// <summary>
        /// Gets a data object which is equivalent for the specified entity object.
        /// </summary>
        /// <param name="entityObject">The entity object.</param>
        /// <param name="session">The NHibernate session.</param>
        /// <returns>The created data object.</returns>
        protected abstract TData GetDataObjectEntity(TEntity entityObject, ISession session);

        /// <summary>
        /// Prepares and validates the specified entity object before saving it to the database.
        /// </summary>
        /// <param name="entityObject">The entity object to save.</param>
        /// <param name="session">The NHibernate session.</param>
        protected virtual void PrepareEntityForSave(TEntity entityObject, ISession session)
        {
        }

        /// <summary>
        /// Initializes a new entity object after it has been saved to the database. Most of all, this method must
        /// save the identifier of the saved object from the data object to the entity object.
        /// </summary>
        /// <param name="obj">The entity object.</param>
        /// <param name="data">The data object.</param>
        /// <param name="session">The NHibernate session.</param>
        protected abstract void InitializeSavedEntity(TEntity obj, TData data, ISession session);

        /// <summary>
        /// Handles custom query parts in the save fluent query.
        /// </summary>
        /// <param name="entityId">The identifier of the object to save.</param>
        /// <param name="reader">The fluent query reader instance for the save query.</param>
        /// <returns><c>true</c> if the save operation was handled; otherwise, <c>false</c>.</returns>
        [ExcludeFromCodeCoverage]
        protected virtual bool HandleSaveOperations(TId entityId, FluentQueryReader reader)
        {
            if (this.UsingSession(session => this.HandleSaveOperations(entityId, reader, session)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles custom query parts in the save fluent query.
        /// </summary>
        /// <param name="entityId">The identifier of the object to save.</param>
        /// <param name="reader">The fluent query reader instance for the save query.</param>
        /// <param name="session">The NHibernate session.</param>
        /// <returns><c>true</c> if the save operation was handled; otherwise, <c>false</c>.</returns>
        protected virtual bool HandleSaveOperations(TId entityId, FluentQueryReader reader, ISession session)
        {
            return false;
        }
    }
}