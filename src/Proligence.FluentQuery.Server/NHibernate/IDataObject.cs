﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataObject.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server.NHibernate
{
    /// <summary>
    /// Defines the API for wrapper objects which contain data returned by NHibernate queries.
    /// </summary>
    /// <typeparam name="TEntity">
    /// The type of object which represents the entity in the application's logic tier.
    /// </typeparam>
    /// <typeparam name="TId">
    /// The type which represents the unique identifier of the entity objects.
    /// </typeparam>
    public interface IDataObject<out TEntity, out TId>
    {
        /// <summary>
        /// Gets the unique identifier of the object.
        /// </summary>
        TId Id { get; }

        /// <summary>
        /// Creates a new logic-tier object which contains the data of this object.
        /// </summary>
        /// <returns>The created logic-tier object.</returns>
        TEntity ToEntity();
    }
}