﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFluentQueryRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server.NHibernate
{
    using System.Collections.Generic;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Defines the interface for classes which implement fluent-based object repositories.
    /// </summary>
    /// <typeparam name="TObject">The type of objects stored in the repository.</typeparam>
    /// <typeparam name="TId">The type of identifier of the objects in the repository.</typeparam>
    public interface IFluentQueryRepository<out TObject, out TId>
    {
        /// <summary>
        /// Creates or updates the specified object.
        /// </summary>
        /// <param name="query">The fluent query which specifies the object to save.</param>
        /// <returns>The identifier of the saved object.</returns>
        TId Save(QueryPart query);

        /// <summary>
        /// Gets the objects returned by the specified fluent query.
        /// </summary>
        /// <param name="query">The fluent query which specifies the objects to get.</param>
        /// <returns>A sequence of objects returned by the fluent query.</returns>
        IEnumerable<TObject> GetObjects(QueryPart query);

        /// <summary>
        /// Gets the identifiers of the objects returned by the specified fluent query.
        /// </summary>
        /// <param name="query">The fluent query which specifies the objects to get.</param>
        /// <returns>A sequence of identifiers of the objects returned by the fluent query.</returns>
        IEnumerable<TId> GetIds(QueryPart query);
    }
}