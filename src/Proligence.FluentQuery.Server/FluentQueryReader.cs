﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryReader.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server
{
    using System.Collections.Generic;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements helper methods for reading fluent queries.
    /// </summary>
    public class FluentQueryReader : FluentQueryReaderBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FluentQueryReader"/> class.
        /// </summary>
        /// <param name="queryRootPart">The root part of the fluent query.</param>
        public FluentQueryReader(QueryPart queryRootPart)
            : base(queryRootPart)
        {
        }

        /// <summary>
        /// Gets the specified parts of the fluent query.
        /// </summary>
        /// <typeparam name="TPart">The type of the query parts to get.</typeparam>
        /// <param name="name">The name of the query parts to get or <c>null</c>.</param>
        /// <returns>A sequence which contains the specified query parts.</returns>
        public override IEnumerable<TPart> GetQueryParts<TPart>(string name = null)
        {
            QueryPart currentPart = this.QueryRootPart;
            while (currentPart != null)
            {
                TPart part = currentPart as TPart;
                if (part != null)
                {
                    if (!string.IsNullOrEmpty(name))
                    {
                        if (currentPart.Name == name)
                        {
                            yield return part;
                        }
                    }
                    else
                    {
                        yield return part;
                    }
                }

                currentPart = currentPart.ParentPart;
            }
        }
    }
}