﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryValidatorBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server
{
    using System;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// The base class for classes which implement validating fluent queries.
    /// </summary>
    public abstract class FluentQueryValidatorBase
    {
        /// <summary>
        /// Validates the specified fluent query and throws an exception if the query is invalid.
        /// </summary>
        /// <param name="queryRootPart">The root part of the fluent query.</param>
        public void Validate(QueryPart queryRootPart)
        {
            if (queryRootPart == null)
            {
                throw new ArgumentNullException("queryRootPart");
            }

            for (QueryPart queryPart = queryRootPart; queryPart != null; queryPart = queryPart.ParentPart)
            {
                if (!this.ValidateQueryPart(queryPart))
                {
                    throw new QueryPartException(
                        "The fluent query contains an invalid or unsupported query part '" + queryPart.Name + "'.",
                        queryPart.GetType(),
                        queryPart.Name);
                }   
            }
        }

        /// <summary>
        /// Validates the specified fluent query part.
        /// </summary>
        /// <param name="queryPart">The query part to validate.</param>
        /// <returns><c>true</c> if the query part is valid; otherwise, <c>false</c>.</returns>
        protected abstract bool ValidateQueryPart(QueryPart queryPart);
    }
}