﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryNameValidator.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server
{
    using System;
    using System.Collections.Generic;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements a fluent query validator that ensures that the validated fluent queries contain only unnamed query
    /// parts or query parts with names from the specified predefined list of names.
    /// </summary>
    public class FluentQueryNameValidator : FluentQueryValidatorBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FluentQueryNameValidator"/> class.
        /// </summary>
        public FluentQueryNameValidator()
        {
            this.ValidNames = new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FluentQueryNameValidator"/> class.
        /// </summary>
        /// <param name="supportedNames">The valid query part names.</param>
        public FluentQueryNameValidator(IEnumerable<string> supportedNames)
        {
            this.ValidNames = new List<string>(supportedNames);
        }

        /// <summary>
        /// Gets the list of valid query part names.
        /// </summary>
        protected IList<string> ValidNames { get; private set; }

        /// <summary>
        /// Adds the specified name to the list of valid query part names.
        /// </summary>
        /// <param name="name">The name to add to the list of valid query part names.</param>
        public void AddQueryPartName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            if (!this.ValidNames.Contains(name))
            {
                this.ValidNames.Add(name);
            }
        }

        /// <summary>
        /// Adds the specified names to the list of valid query part names.
        /// </summary>
        /// <param name="names">The names to add to the list of valid query part names.</param>
        public void AddQueryPartNames(IEnumerable<string> names)
        {
            if (names == null)
            {
                throw new ArgumentNullException("names");
            }

            foreach (string name in names)
            {
                if (!this.ValidNames.Contains(name))
                {
                    if (name == null)
                    {
                        throw new ArgumentException("The specified names collection contains a null value.", "names");
                    }

                    this.ValidNames.Add(name);
                }
            }
        }

        /// <summary>
        /// Validates the specified fluent query part.
        /// </summary>
        /// <param name="queryPart">The query part to validate.</param>
        /// <returns><c>true</c> if the query part is valid; otherwise, <c>false</c>.</returns>
        protected override bool ValidateQueryPart(QueryPart queryPart)
        {
            if (queryPart == null)
            {
                throw new ArgumentNullException("queryPart");
            }

            if (queryPart.Name != null)
            {
                if (!this.ValidNames.Contains(queryPart.Name))
                {
                    return false;
                }
            }

            return true;
        }
    }
}