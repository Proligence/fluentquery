﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryReaderBaseExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Server
{
    using System;
    using System.Collections.Generic;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Implements extension methods for the <see cref="FluentQueryReaderBase"/> class.
    /// </summary>
    public static class FluentQueryReaderBaseExtensions
    {
        /// <summary>
        /// Gets the value of the specified <see cref="SwitchQueryPart"/> of the fluent query.
        /// </summary>
        /// <param name="reader">The <see cref="FluentQueryReaderBase"/> instance.</param>
        /// <param name="name">The name of the <see cref="SwitchQueryPart"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="SwitchQueryPart.Enabled"/> value of the specified <see cref="SwitchQueryPart"/> or
        /// <c>false</c> if the specified query part was not found.
        /// </returns>
        public static bool GetSwitch(this FluentQueryReaderBase reader, string name)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            return reader.GetSwitch<SwitchQueryPart>(name);
        }

        /// <summary>
        /// Gets the value of the specified <see cref="ValueQueryPart{T}"/> of the fluent query.
        /// </summary>
        /// <typeparam name="TValue">
        /// The type of the value stored in the specified <see cref="ValueQueryPart{T}"/>.
        /// </typeparam>
        /// <param name="reader">The <see cref="FluentQueryReaderBase"/> instance.</param>
        /// <param name="name">The name of the <see cref="ValueQueryPart{T}"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="ValueQueryPart{T}.Value"/> value of the specified <see cref="ValueQueryPart{T}"/> or
        /// <c>default(<typeparamref name="TValue"/>)</c> if the specified query part was not found.
        /// </returns>
        public static TValue GetValue<TValue>(this FluentQueryReaderBase reader, string name)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            return reader.GetValue<ValueQueryPart<TValue>, TValue>(name);
        }

        /// <summary>
        /// Gets the value of the specified <see cref="ValueQueryPart{T}"/> of the fluent query and throws an exception
        /// if there is no value query part with the specified name.
        /// </summary>
        /// <typeparam name="TValue">
        /// The type of the value stored in the specified <see cref="ValueQueryPart{T}"/>.
        /// </typeparam>
        /// <param name="reader">The <see cref="FluentQueryReaderBase"/> instance.</param>
        /// <param name="name">The name of the <see cref="ValueQueryPart{T}"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="ValueQueryPart{T}.Value"/> value of the specified <see cref="ValueQueryPart{T}"/> or
        /// <c>default(<typeparamref name="TValue"/>)</c> if the specified query part was not found.
        /// </returns>
        public static TValue GetRequiredValue<TValue>(this FluentQueryReaderBase reader, string name)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            var queryPart = reader.GetQueryPart<ValueQueryPart<TValue>>(name);
            if (queryPart == null)
            {
                throw new MissingQueryPartException(typeof(ValueQueryPart<TValue>), name);
            }

            return queryPart.Value;
        }

        /// <summary>
        /// Gets the value of the specified <see cref="ListQueryPart{T}"/> of the fluent query.
        /// </summary>
        /// <typeparam name="TItem">
        /// The type of items stored in the specified <see cref="ListQueryPart{T}"/>.
        /// </typeparam>
        /// <param name="reader">The <see cref="FluentQueryReaderBase"/> instance.</param>
        /// <param name="name">The name of the <see cref="ListQueryPart{T}"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="ListQueryPart{T}.Values"/> value of the specified <see cref="ListQueryPart{T}"/> or
        /// <c>null</c> if the specified query part was not found.
        /// </returns>
        public static IEnumerable<TItem> GetList<TItem>(this FluentQueryReaderBase reader, string name)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            return reader.GetList<ListQueryPart<TItem>, TItem>(name);
        }

        /// <summary>
        /// Gets the value of the specified <see cref="ListQueryPart{T}"/> of the fluent query.
        /// </summary>
        /// <typeparam name="TItem">
        /// The type of items stored in the specified <see cref="ListQueryPart{T}"/>.
        /// </typeparam>
        /// <param name="reader">The <see cref="FluentQueryReaderBase"/> instance.</param>
        /// <param name="name">The name of the <see cref="ListQueryPart{T}"/> or <c>null</c>.</param>
        /// <returns>
        /// The <see cref="ListQueryPart{T}.Values"/> value of the specified <see cref="ListQueryPart{T}"/> or
        /// <c>null</c> if the specified query part was not found.
        /// </returns>
        public static IEnumerable<TItem> GetRequiredList<TItem>(this FluentQueryReaderBase reader, string name)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            var queryPart = reader.GetQueryPart<ListQueryPart<TItem>>(name);
            if (queryPart == null)
            {
                throw new MissingQueryPartException(typeof(ListQueryPart<TItem>), name);
            }

            return queryPart.Values;
        }
    }
}