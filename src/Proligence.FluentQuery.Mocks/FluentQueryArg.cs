﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FluentQueryArg.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.FluentQuery.Mocks
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Moq;
    using Proligence.FluentQuery.Client.Contract;

    /// <summary>
    /// Represents fluent query arguments in mock expectations.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class FluentQueryArg
    {
        /// <summary>
        /// Helper methods for mocking fluent queries.
        /// </summary>
        /// <typeparam name="TQuery">The type of expected query.</typeparam>
        /// <param name="expectedQuery">The expected query.</param>
        /// <returns>The mock argument.</returns>
        public static FluentQuery<TQuery> Is<TQuery>(FluentQuery<TQuery> expectedQuery)
            where TQuery : QueryPart, new()
        {
            if (expectedQuery == null)
            {
                throw new ArgumentNullException("expectedQuery");
            }

            return Match.Create<FluentQuery<TQuery>>(
                actualQuery => expectedQuery(new TQuery()).Equals(actualQuery(new TQuery()), true));
        }
    }
}
